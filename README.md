# CloudLightning Simulator Visualization

## Getting started

Clone project:

    https://bitbucket.org/cloudlightning/cl-simulatorvisualization

**Dependencies Installation - Linux users:**

Install Node.js (version 6.x from https://nodejs.org/en/):

    $ curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    $ sudo apt-get install -y nodejs
    
Navigate to the directory of the project and enter:

    $ sudo npm install bower -g
    $ sudo npm install gulp -g
    $ npm install
    
**Dependencies Installation - Windows users:**

Install Node.js (version 6.x from https://nodejs.org/en/).

Through Node.js Command Prompt navigate to the directory of the project and enter:

    > npm install bower -g
    > npm install gulp -g
    > npm set strict-ssl false
    > npm install
    > bower install
    
## Project development

Run development web-server from terminal:

    Navigate to the directory of the project and enter
    $ sudo gulp serve


## Project build

From terminal:

    Navigate to the directory of the project and enter
    $ sudo gulp build

## Deployment in Apache Server

    $ Copy "dist" folder of the project and paste in DocumentRoot* folder of Apache Server.
    $ From "Project_Folder\src" folder, copy "input_scripts", "input_stylesheets" folders and paste them inside DocumentRoot "\dist" folder of Apache Server.
    $ From "Project_Folder\src" folder, copy "app", "assets" and paste them inside DocumentRoot folder of Apache Server.
    $ Start Apache Server and then open your web browser and type in: http://localhost/dist.

*DocumentRoot folder in Windows might be "/htdocs/" folder and in Linux "/var/www/html" folder.

## Examples
For better understanding of the *CL Simulator Vizualization*, a set of examples is available under the folder named *"examples"* that can be found in the present repository. In particular, three sample input JSON files are provided, along with their corresponding output file that contains the results of the simulation.

You can use the sample files in order to test the *CL Simulator Vizualization*, through the process described below:

- Navigate to the *Input Data* page of the *CL Simulator Vizualization* and import each one of the JSON files that can be found inside the *examples/input* folder to their associated tabs. More specifically, the *AppData.json, CellData.json*, and *BrokerData.json*  should be imported to the *Applications, Cells*, and *Brokers* tabs respectively.
- Navigate to the *Run* tab and click on the button *"Upload Data to Simulator"*.
- After the uploading is completed, click on the *"Run"* button in order to start the simulation.
- The simulation will be triggered and the results will automatically be displayed on the *Visualization* page of the *CL Simulator Vizualization*, as they are being produced by the *Cloudlightning Simulator*.
- Alternatively, you can navigate directly to the *Visualization* page and import an existing JSON file containing results produced by the Simulator in order to display them. Such a sample file can be found inside the *'examples/output"* folder.

## Deliverables
The deliverables of the CloudLightning Project can be found at the following url: http://cloudlightning.eu/work-packages/public-deliverables/

## Acknowledgements
Cloudlightning was funded by the European Commission's Horizon 2020 Programme for Research and Innovation under Grand Agreement No. 643946.