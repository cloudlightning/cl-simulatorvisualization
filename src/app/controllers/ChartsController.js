(function () {
    angular
        .module('app')
        .controller('ChartsController', [
            'ChartsService', 'TidyTreeService', 'BubbleChartService', '$scope', '$interval', '$window',
            'jsonInfo', 'BubbleTooltipService', 
            ChartsController
        ]);

    function ChartsController(ChartsService, TidyTreeService, BubbleChartService, $scope, $interval, $window
    , jsonInfo, BubbleTooltipService
    ) {
        var vm = this;

        vm.show = false;

        $scope.sizes = [];
        
        //$scope.selectedSizes = [];
        vm.selectedTitles = ['Total Energy Consumption', 'Processor Utilization over Active Servers', 'Accelerator Utilization over Active Servers', 'Total Number of accepted Tasks'];
        vm.toggle = function (item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
            list.splice(idx, 1);
            }
            else {
            list.push(item);
            }
        };

        vm.exists = function (item, list) {
            return list.indexOf(item) > -1;
        };

        vm.isIndeterminate = function() {
            return (vm.selectedTitles.length !== 0 &&
                vm.selectedTitles.length !== vm.lineChartTitles2.length);
          };
        
        vm.isChecked = function() {
            return vm.selectedTitles.length === vm.lineChartTitles2.length;
          };
        
        vm.toggleAll = function() {
            if (vm.selectedTitles.length === vm.lineChartTitles2.length) {
                vm.selectedTitles = [];
            } else if (vm.selectedTitles.length === 0 || vm.selectedTitles.length > 0) {
                vm.selectedTitles = vm.lineChartTitles2.slice(0);
            }
          };

        vm.isNull = function (value){
            if (value == null){
                return true;
            } else {
                return false;
            }
        }

        vm.fileIsBig = function (){
            if (vm.timelength > 500){
                return true;
            }
        }


         // radio buttons
        vm.speed = 1000;
        vm.hasChanged = function (){
            
            // clearInterval(p);
            // p = setInterval(updateCharts, vm.speed);
            $interval.cancel(p);
            p = $interval(updateCharts, vm.speed);
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                vm.chartsOptions[vm.lineChartTitles[n]].chart.duration = vm.speed;
            } 
        };
       
        // show progress when simulator is running (before producing any results)
        vm.showList = [ ];
        vm.showList[1] = true;
           

        // selection panel for bubble graph
        vm.bubbleChartData = [];
        vm.selectionNameHasChanged = function(value){
            $scope.selectedUtilizationName =value;
            if (vm.myBubbleChart){
                vm.svgBubble = d3.select(".inner_bubble");
                vm.svgBubble.selectAll("*").remove();
                // re-create raw data
                vm.bubbleChartData = [];
                createBubbleChartData();
                // re-create chart
                var bnames =[];
                for (var s=0; s< $scope.sizes.length-1; s++){
                    bnames.push($scope.sizes[s].name);
                }
                vm.myBubbleChart = BubbleChartService.bubbleChart(bnames, BubbleTooltipService);
                vm.myBubbleChart('.inner_bubble', vm.bubbleChartData);
            }
            
        };

        //if window.isRunning is not defined make it false
        if (!$window.isRunning){
            $window.isRunning = false;
        }
        vm.simulatorIsRunning = function(){
            return $window.isRunning;
        }
        
        vm.startpos = 0;
        vm.outputFileSize =0;
        var a=1;
        $window.myGlobalAjax = function(){
            var ajaxCallstartTime = performance.now();
            
           $.ajax({
            type: "POST",
            url: 'php/download_file.php',
            data:{size:vm.outputFileSize, sessionId:$window.sessionId},
            global: true,
            success:function(res, status,request) {
              //console.log('Success!' + res);


                var ajaxCallTime = performance.now() - ajaxCallstartTime;
                $window.serverRespondedToRequest = true;
                console.log('Success!');
                console.log(request.getAllResponseHeaders());
                
                var startTime = performance.now();
              
                var result = JSON.parse(res);
                
                
                if (vm.outputFileSize < result['size']){
                    vm.outputFileSize = result['size'];
                    console.log('New size' + vm.outputFileSize);
                   
                    
                    if(result['content'] && result['content']!=false) {
                        jsonData = JSON.parse(result['content']);
                        computeJsonData2(jsonData); 
                    }
                } else if (vm.outputFileSize == result['size']){
                    console.log('Same size');
                } else {
                    console.log('Smaller size');
                }
                
                
                var endTime = performance.now();
                console.log('File download' + result['extime'] + ' s.'+ '\nMyajax call to server took:' + ajaxCallTime + ' ms' +'\nMyajax processing took:' + (endTime-startTime) + ' ms\n');
              
            },
            error:function(html) {
                alert('Error!' + html);
              }
 
       });
        }

        vm.generalInfo = [{name:"Resource allocation mechanism", value:null}, {name:"Number of Cells", value:null}, {name:"Total simulation time (s)", value:null}, {name:"Total Energy Consumption (MWh)", value:null},
        {name:"Total number of submitted tasks", value:null}, {name:"Total Number of accepted Tasks", value:null},{name:"Total Number of rejected Tasks", value:null},{name:"Average Processor Utilization over active servers", value:null}, {name:"Average Processor Utilization", value:null},
        {name:"Average Memory Utilization over active servers", value:null}, {name:"Average Memory Utilization", value:null}, {name:"Average Network Utilization", value:null},
        {name:"Average Storage Utilization over active servers", value:null}, {name:"Average Storage Utilization", value:null},
        {name:"Average Accelerator Utilization over active servers", value:null}, {name:"Average Accelerator Utilization", value:null}];

        //line charts initialization
        vm.lineChartTitles2 = ['Total Energy Consumption', 'Processor Utilization over Active Servers', 'Processor Utilization', 'Memory Utilization over Active Servers',
        'Memory Utilization', 'Network Utilization', 'Storage Utilization over Active Servers', 'Storage Utilization',
        'Accelerator Utilization over Active Servers', 'Accelerator Utilization', 'Active Servers', 'Total Number of accepted Tasks', 'Total Number of rejected Tasks','Running VMs'];
        vm.lineChartTitles = ['Total Energy Consumption', 'Processor Utilization over Active Servers', 'Processor Utilization', 'Memory Utilization over Active Servers',
        'Memory Utilization', 'Network Utilization', 'Storage Utilization over Active Servers', 'Storage Utilization',
        'Accelerator Utilization over Active Servers', 'Accelerator Utilization', 'Active Servers', 'Total Number of accepted Tasks', 'Total Number of rejected Tasks', 'Running VMs'];
        var lineChartNames = ['Energy Consumption (MWh)', 'Processor Utilization over Active Servers (%)', 'Processor Utilization (%)', 'Memory Utilization over Active Servers (%)',
        'Memory Utilization (%)', 'Network Utilization (%)', 'Storage Utilization over Active Servers (%)', 'Storage Utilization (%)',
        'Accelerator Utilization over Active Servers (%)', 'Accelerator Utilization (%)', 'Active Servers', 'Number of accepted Tasks', 'Total Number of rejected Tasks','Running VMs'];
        var lineChartTickFormats = ['.01f', '.02f', '.02f', '.02f', '.02f', '.02f', '.02f', '.02f', '.02f', '.02f', '.0f', '.0f', '.0f','.0f'];
        var symbol = ['MWh', '%', '%', '%', '%', '%', '%', '%', '%', '%', '', '', '', ''];
        vm.default_colors = ['#3366CC','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395',
                            '#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC'];
        vm.utilizationNames = ['Processor Utilization over Active Servers', 'Processor Utilization', 'Memory Utilization over Active Servers',
        'Memory Utilization', 'Network Utilization', 'Storage Utilization over Active Servers', 'Storage Utilization',
        'Accelerator Utilization over Active Servers', 'Accelerator Utilization'];
                          
        //chart options
        vm.chartsOptions = {};
        // chart data
        $scope.data = {};
        
        // set all the line charts options (title, axis etc.)
        for (var n=0 ; n < vm.lineChartTitles.length ; n++){
            var singleChartOptions = ChartsService.chartOptions(vm.lineChartTitles[n], lineChartNames[n], lineChartTickFormats[n], vm.speed);
            vm.chartsOptions[vm.lineChartTitles[n]] = singleChartOptions;
           // vm.data[vm.lineChartTitles[n]] = []; // do this inside initialize()
        } 

         ///////////// canvas//////////////////////////////

        var canvas = [];
        $scope.cdata = {};
        for (var n=0 ; n < vm.lineChartTitles.length ; n++){
            canvas[n] = document.getElementById(vm.lineChartTitles[n]);
            canvas[n].addEventListener('dblclick', function(){ 

            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                myLineChart[vm.lineChartTitles[n]].resetZoom();
            } 

            });
            $scope.cdata[vm.lineChartTitles[n]] = {labels: [], datasets: []};
        }

        // canvas chart options
        vm.canvasChartsOptions = {};
        for (var n=0 ; n < vm.lineChartTitles.length ; n++){
            var singleCanvasChartOptions = ChartsService.canvasChartOptions(vm.lineChartTitles2[n], lineChartNames[n]);
            vm.canvasChartsOptions[vm.lineChartTitles[n]] = singleCanvasChartOptions;
        } 

        var myLineChart = {};
        setTimeout(function(){
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                myLineChart[vm.lineChartTitles[n]] = new Chart(canvas[n], {
                    type: 'line',
                    data: $scope.cdata[vm.lineChartTitles[n]],
                    options: vm.canvasChartsOptions[vm.lineChartTitles[n]]
                });
            }
            
        }, 0);

		////////////////////// canvas//////////////////////////////////
       
       // initialize graph data
       initialize();
        
        
        // get json data
        var jsonData = null;
        var res;
       

        
		var fileInput = document.getElementById('fileinput');
        var fileDisplayArea = document.getElementById('fileDisplayArea');
        var progress = document.querySelector('.percent');
        var progress_bar = document.getElementById('progress_bar');

		fileInput.onchange = function (e) {
			var file = fileInput.files[0];
            if (fileInput.files.length != 0){
                var extension = file.name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = extension == 'json';  //is extension in acceptable types
                if (!window.FileReader || !window.FileList || !window.Blob) {      
                    alert('The File APIs are not fully supported in this browser.');
                }
                if (isSuccess) {
                    var reader = new FileReader();
                    
                    initialize();
                    //fileDisplayArea.innerText = "";
                    
                    reader.onerror = function (e) {
                        fileDisplayArea.innerText = "Error occured: " + e.target.error.name ;
                    };
                    reader.onloadstart = function(e) {
                        
                        progress_bar.className = 'loading';
                    };
                    reader.onprogress = function (e) {
                        // evt is an ProgressEvent.
                        if (e.lengthComputable) {
                        var percentLoaded = Math.round((e.loaded / e.total) * 100);
                        // Increase the progress bar length.
                        if (percentLoaded < 100) {
                            //fileDisplayArea.innerText = percentLoaded + "% is loaded."
                            progress.style.width = percentLoaded + '%';
                            progress.textContent = percentLoaded + '%';
                        }
                        }
                    };
                    
                    reader.onload = function(e) {
                        res = e.target.result;

                        
                            console.log(res);
                            try {
                                jsonData = JSON.parse(res);
                            } catch (e) {
                                vm.show = false;
                                console.log(e);
                                alert('Invalid json format!');
                                 // hide progress bar if it is invalid
                                progress_bar.className = 'percent';
                                return;
                            }
                        
                
                            // Ensure that the progress bar displays 100% at the end.
                            progress.style.width = '100%';
                            progress.textContent = '100%';
                            // hide progress bar after it reaches 100%
                            setTimeout(function(){
                            progress_bar.className = 'percent';
                            }, 0);
                            //fileDisplayArea.innerText = "\"" + file.name + "\" file is loaded!";
                
                            $scope.reset(); 
                            computeJsonData(jsonData);   
                    
                    }

                    reader.readAsText(file);
                    
                    
                    
                } else {
                    alert('File not supported! Only .json files are allowed!');
                }
            }
            //Clear the list with the files (to allow re-importation of the same file)
		    document.getElementById('fileinput').value = "";
		};
      //  }
        
        function computeJsonData(jsonData){
            
            try{
                var version = "1";
                var length = 0;
                if (jsonData["Resource allocation mechanism"]){
                    version = "2";
                    vm.generalInfo[0].value = jsonData["Resource allocation mechanism"];
                    var submitted_tasks_real = jsonData["Total number of submitted tasks"];
                    vm.generalInfo[4].value = numberWithCommas(submitted_tasks_real);
                    length= jsonData["CLSim outputs"].length;
                } else {
                    length = jsonData.length;
                }
                
                var i=0,l=length;
                var obj = {};
                var names =[];
            
                // create an object for each cell
                for(i;i<l;i++) {
                    var datum;
                    if (version === "1"){
                        datum = jsonData[i];
                    } else {
                        datum = jsonData["CLSim outputs"][i];
                    }
                    
                    console.log(datum["Cell"]);
                    var name = "Cell-"+ datum["Cell"];
                    if(obj[name]) { 
                    obj[name].push(datum);
                    }
                    else {
                        obj[name] = [];
                        obj[name].push(datum);
                        
                        names.push(name);
                    }
                }
            
                // values for select button
                for (name in names){
                    $scope.sizes.push({category: 'cells', name:names[name]});
                }
                $scope.sizes.push({category:'cloud', name: 'Cloud'}); 
            
                    
                var data3 = $scope.data3;
                vm.chartObject = jsonInfo.getChartMaps(names, obj, data3, vm.lineChartTitles);
                vm.chartMaps = vm.chartObject.chartMaps;
                // vm.chart1map = vm.chartObject.chartmapEnergy;
                // vm.chart2map = vm.chartObject.chartmapUtilization;
                $scope.data3 = vm.chartObject.data3;
                vm.timelength = vm.chartObject.timelength;

                // averages
                // Num of Cells
                vm.generalInfo[1].value =  names.length;
                // Total time
                vm.generalInfo[2].value =  numberWithCommas(vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][vm.timelength -1].t);
                // total energy consumption
                var avgValueEnergy = numberWithCommas(Math.round( (vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][vm.timelength -1].y) * 100 ) / 100);
                if (avgValueEnergy< 1){
                    avgValueEnergy = vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][vm.timelength -1].y.toExponential(3);
                } 
                vm.generalInfo[3].value =  avgValueEnergy;
                //total number of accepted tasks
                var accepted_tasks_real= vm.chartObject.chartMaps[vm.lineChartTitles[11]]['Cloud'][vm.timelength -1].y;
                vm.generalInfo[5].value =   numberWithCommas(accepted_tasks_real);
                //total number of rejected tasks
                vm.generalInfo[6].value = numberWithCommas(submitted_tasks_real - accepted_tasks_real);
                //vm.generalInfo[6].value =   numberWithCommas(vm.chartObject.chartMaps[vm.lineChartTitles[12]]['Cloud'][vm.timelength -1].y);
                
                // avg utilization
                for (var i=1; i<10 ; i++){
                    var avgValue = Math.round( (vm.chartObject.utilizationCloudAverage[vm.lineChartTitles[i]]) * 100 ) / 100 ;
                    if (avgValue < 1){
                        avgValue = vm.chartObject.utilizationCloudAverage[vm.lineChartTitles[i]];
                        if (avgValue != 0){
                            avgValue = avgValue.toExponential(3);
                        }
                        vm.generalInfo[i+6].value =  avgValue + ' %';
                    } else {
                        vm.generalInfo[i+6].value =  avgValue + ' %';
                    }
                    
                }

                //bubble chart data
                vm.bubbleChartData = [];
                createBubbleChartData();
               
                
                // time step
                vm.timeStep = vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][2].t - vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][1].t;

                // 3 buttons
                vm.startTime1 = 0;
                vm.endTime1 = numberWithCommas(vm.timeStep * Math.round(vm.timelength/3) -1);
                vm.startTime2 = numberWithCommas(vm.timeStep * (Math.round(vm.timelength/3) + 1) -1);
                vm.endTime2 = numberWithCommas(vm.timeStep * Math.round(2*vm.timelength/3) - 1);
                vm.startTime3 = numberWithCommas(vm.timeStep *(Math.round(2* vm.timelength/3) + 1) - 1);
                vm.endTime3 = numberWithCommas(vm.timeStep * (vm.timelength - 1) -1);

        
                // ************** Generate the tree diagram	 *****************
                generateTidyTree(names);
                
            

                //***************** Create bubble graph **************************//
                /*
                * Below is the initialization code as well as some helper functions
                * to create a new bubble chart instance, load the data, and display it.
                */
                
                vm.myBubbleChart = BubbleChartService.bubbleChart(names, BubbleTooltipService);
                vm.myBubbleChart('.inner_bubble', vm.bubbleChartData);
                // setup the buttons.
                //BubbleChartService.setupButtons(vm.myBubbleChart);
                vm.show = true;
            }catch(e){
                vm.show = false;
                console.log(e);
                alert('An error occured while loading your file. The file does not have a valid schema.')
            }
        }

        function computeJsonData2(jsonData){
            try{
                var version = "1";
                var length = 0;
                if (jsonData["Resource allocation mechanism"]){
                    version = "2";
                    vm.generalInfo[0].value = jsonData["Resource allocation mechanism"];
                    var submitted_tasks_real = jsonData["Total number of submitted tasks"];
                    vm.generalInfo[4].value = numberWithCommas(submitted_tasks_real);
                    length= jsonData["CLSim outputs"].length;
                } else {
                    length = jsonData.length;
                }
                
                var i=0,l=length;
                var obj = {};
                var names =[];
            
                // create an object for each cell
                for(i;i<l;i++) {
                    var datum;
                    if (version === "1"){
                        datum = jsonData[i];
                    } else {
                        datum = jsonData["CLSim outputs"][i];
                    }
                    
                    console.log(datum["Cell"]);
                    var name = "Cell-"+ datum["Cell"];
                    if(obj[name]) { 
                    obj[name].push(datum);
                    }
                    else {
                        obj[name] = [];
                        obj[name].push(datum);
                        
                        names.push(name);
                    }
                }
            
                // values for select button
                if (vm.startpos == 0){
                    initialize();
                    $scope.reset();
                    console.log('Initialize\n');
                    console.log($scope.data3);
                    for (name in names){
                        $scope.sizes.push({category: 'cells', name:names[name]});
                    }
                    $scope.sizes.push({category:'cloud', name: 'Cloud'}); 
                }
                    
                
                vm.treeMap = jsonInfo.treeMapInit(vm.lineChartTitles);
                var data3 = [{name: "Cloud",
                            parent: null,
                            treeMap: vm.treeMap,   
                            processorUtilization: [],
                            children:[],
                            currentValue: null,
                            currentSymbol: "",
                            currentIndex : null
                }];
                
                vm.chartObject = jsonInfo.getChartMaps(names, obj, data3, vm.lineChartTitles);
                var tempChartMaps = vm.chartObject.chartMaps;
                var tempData3 = vm.chartObject.data3;
                
                vm.lastpos = vm.chartObject.timelength;
                
                console.log(vm.chartMaps);
                
                for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                    if (!vm.chartMaps[vm.lineChartTitles[n]]){
                        vm.chartMaps[vm.lineChartTitles[n]] = {};
                    }
                    for (var l=0 ; l<$scope.sizes.length ; l++){
                        
                        if (!vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name]){
                            console.log('ChartMap['+ [vm.lineChartTitles[n]]+','+ [$scope.sizes[l].name]+'] created\n');
                            vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name]=[];
                        }
                        for (var pos=vm.startpos; pos<vm.lastpos;pos++){
                            //console.log(tempChartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name]);
                            vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name].push({t:tempChartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name][pos].t,y:tempChartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name][pos].y});
                        }
                    }
                }
                console.log('Start:' + vm.startpos + ', End:'+ vm.lastpos + '\n');
                for (var pos=vm.startpos; pos<vm.lastpos;pos++){
                    // cloud node
                    for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                        $scope.data3[0].treeMap[vm.lineChartTitles[n]].push({t:tempData3[0].treeMap[vm.lineChartTitles[n]][pos].t,value:tempData3[0].treeMap[vm.lineChartTitles[n]][pos].value});
                    }
                    // cell nodes
                    for (var i=0 ; i< tempData3[0].children.length; i++){
                        if (!$scope.data3[0].children[i]){
                            $scope.data3[0].children[i] ={name: tempData3[0].children[i].name, parent: "Cloud", treeMap: {}, 
                            processorUtilization: [], children: [], currentValue: null, currentIndex: null};
                            $scope.data3[0].children[i].treeMap = jsonInfo.treeMapInit(vm.lineChartTitles);
                        }
                        for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                            $scope.data3[0].children[i].treeMap[vm.lineChartTitles[n]].push({t:tempData3[0].children[i].treeMap[vm.lineChartTitles[n]][pos].t,value:tempData3[0].children[i].treeMap[vm.lineChartTitles[n]][pos].value});
                        }
                        // hw nodes
                        for (var j=0 ; j< tempData3[0].children[i].children.length; j++){
                            if (!$scope.data3[0].children[i].children[j]){
                                $scope.data3[0].children[i].children[j]={name: tempData3[0].children[i].children[j].name, parent: tempData3[0].children[i].name, treeMap: {}, processorUtilization: [], children: [], currentValue:null, currentIndex: null};
                                $scope.data3[0].children[i].children[j].treeMap = jsonInfo.treeMapInit(vm.lineChartTitles);
                            }
                            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                                $scope.data3[0].children[i].children[j].treeMap[vm.lineChartTitles[n]].push({t:tempData3[0].children[i].children[j].treeMap[vm.lineChartTitles[n]][pos].t,value:tempData3[0].children[i].children[j].treeMap[vm.lineChartTitles[n]][pos].value});
                            }
                        }
                    }
                    
                }
                
                vm.timelength = vm.chartObject.timelength;
                //update array size for streaming data in InputData.js
                $window.output_array_size = vm.chartObject.timelength;
                // averages
                // Num of Cells
                vm.generalInfo[1].value =  names.length;
                // Total time
                vm.generalInfo[2].value =  numberWithCommas(vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][vm.timelength -1].t);
                // total energy consumption
                var avgValueEnergy = numberWithCommas(Math.round( (vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][vm.timelength -1].y) * 100 ) / 100);
                if (avgValueEnergy< 1){
                    avgValueEnergy = vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][vm.timelength -1].y.toExponential(3);
                } 
                vm.generalInfo[3].value =  avgValueEnergy;
                //total number of accepted tasks
                var accepted_tasks_real= vm.chartObject.chartMaps[vm.lineChartTitles[11]]['Cloud'][vm.timelength -1].y;
                vm.generalInfo[5].value =   numberWithCommas(accepted_tasks_real);
                //total number of rejected tasks
                vm.generalInfo[6].value = numberWithCommas(submitted_tasks_real - accepted_tasks_real);
                //vm.generalInfo[6].value =   numberWithCommas(vm.chartObject.chartMaps[vm.lineChartTitles[12]]['Cloud'][vm.timelength -1].y);
                
                // avg utilization
                for (var i=1; i<10 ; i++){
                    var avgValue = Math.round( (vm.chartObject.utilizationCloudAverage[vm.lineChartTitles[i]]) * 100 ) / 100 ;
                    if (avgValue < 1){
                        avgValue = vm.chartObject.utilizationCloudAverage[vm.lineChartTitles[i]];
                        if (avgValue != 0){
                            avgValue = avgValue.toExponential(3);
                        }
                        vm.generalInfo[i+6].value =  avgValue + ' %';
                    } else {
                        vm.generalInfo[i+6].value =  avgValue + ' %';
                    }
                    
                }

                // ************** Generate the tree diagram	 *****************
                console.log(vm.startpos);
                if (vm.startpos == 0){
                    console.log('I m in! Generate tree!');
                    generateTidyTree(names);

                    ////////////////////////////////////////////////////////////////////test////////////////////////////
                    console.log('Let s make some bubbles');
                    //bubble chart data
                    vm.bubbleChartData = [];
                    createBubbleChartData();
                    vm.svgBubble = d3.select(".inner_bubble");

                    //***************** Create bubble graph **************************//
                    /*
                    * Below is the initialization code as well as some helper functions
                    * to create a new bubble chart instance, load the data, and display it.
                    */
                    
                    vm.myBubbleChart = BubbleChartService.bubbleChart(names, BubbleTooltipService);
                    vm.myBubbleChart('.inner_bubble', vm.bubbleChartData);
                    ///////////////////////////////////////////////////////////test/////////////////////////////////////////////
                }

                
                for (var pos=vm.startpos; pos<vm.lastpos;pos++){ 
                    plot(pos);
                }
                
                if (vm.timelength == $window.max_output_array_size+1){
                    

                    // time step
                    vm.timeStep = vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][2].t - vm.chartObject.chartMaps[vm.lineChartTitles[0]]['Cloud'][1].t;
                
                    // 3 buttons
                    vm.startTime1 = 0;
                    vm.endTime1 = numberWithCommas(vm.timeStep * Math.round(vm.timelength/3) -1);
                    vm.startTime2 = numberWithCommas(vm.timeStep * (Math.round(vm.timelength/3) + 1) -1);
                    vm.endTime2 = numberWithCommas(vm.timeStep * Math.round(2*vm.timelength/3) - 1);
                    vm.startTime3 = numberWithCommas(vm.timeStep *(Math.round(2* vm.timelength/3) + 1) - 1);
                    vm.endTime3 = numberWithCommas(vm.timeStep * (vm.timelength - 1) -1);

                    //***************** Create bubble graph **************************//
                    /*
                    * Below is the initialization code as well as some helper functions
                    * to create a new bubble chart instance, load the data, and display it.
                    */
                    
                    // vm.myBubbleChart = BubbleChartService.bubbleChart(names, BubbleTooltipService);
                    // vm.myBubbleChart('.inner_bubble', vm.bubbleChartData);
                }
                vm.startpos=vm.lastpos;
                vm.show = true;
            }catch(e){
                vm.show = false;
                $window.output_array_size = $window.max_output_array_size+1
                console.log(e);
                alert('An error occured while loading your file. The file does not have a valid schema.')
            }
        }

        vm.hideBubbles = function(){
            if (vm.simulatorIsRunning() && (vm.timelength < $window.max_output_array_size+1)){
                return true;
            } else {
                return false;
            }
        }

        vm.isLoading = function(){
            if (vm.simulatorIsRunning() && vm.outputFileSize==0){
                return true;
            } else {
                return false;
            }
        }

        // set interval of the charts
        vm.x = 0;
        // set a var to show if charts data are empty
        vm.chartsEmpty = true;
        // vm.chartWidth = $('.chartAreaWrapper2').width();
        var p = $interval(updateCharts, vm.speed);
        
        //var p = setInterval(updateCharts, vm.speed);
        
        $scope.run = false;

        $scope.reset = function() {
            $scope.run = false;
            vm.x = 0;
            vm.chartsEmpty = true;
            
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                $scope.data[vm.lineChartTitles[n]] = [];
                //canvas
                myLineChart[vm.lineChartTitles[n]].resetZoom();
                myLineChart[vm.lineChartTitles[n]].data.labels.length =0;
                myLineChart[vm.lineChartTitles[n]].data.datasets.length =0;
                //myLineChart[vm.lineChartTitles[n]].data = {labels: [], datasets: []};
                myLineChart[vm.lineChartTitles[n]].update();
            } 
            vm.time =  "";
            
            TidyTreeService.resetTidyTree(vm.svgTidy, $scope.data3);
        };

        
        // show the 1st, 2nd and last part of json data file.
        $scope.first = function(){
            $scope.run = false;
            vm.x=0;
            vm.chartsEmpty = false;
            showFirst(1);
        }
    
        $scope.second = function(){
            $scope.run = false;
            vm.x=0;
            vm.chartsEmpty = false;
            showFirst(2);
        }

        $scope.third = function(){
            $scope.run = false;
            vm.x=0;
            vm.chartsEmpty = false;
            showFirst(3);
        }
        $scope.entire = function(){
            $scope.run = false;
            vm.x=0;
            vm.chartsEmpty = false;
            showFirst(0);
        }

        $scope.fast_forward = function(){
            $scope.run = false;
            vm.x = 0;
            vm.chartsEmpty = false;
            showFirst(4);
        };


        function showFirst(num){
            
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                vm.chartsOptions[vm.lineChartTitles[n]].chart.zoom.enabled = false;
                myLineChart[vm.lineChartTitles[n]].resetZoom();
                } 
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                $scope.data[vm.lineChartTitles[n]] = [];
            }
            ////canvas
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                myLineChart[vm.lineChartTitles[n]].data.labels.length =0;
                myLineChart[vm.lineChartTitles[n]].data.datasets.length =0;
                //myLineChart[vm.lineChartTitles[n]].data = {labels: [], datasets: []};
            } 
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                if (vm.selectedTitles.indexOf(vm.lineChartTitles2[n]) != -1 || vm.lineChartTitles[n] == $scope.title){
                var lineChartsNumber = $scope.sizes.length;
                for (var l=0 ; l< lineChartsNumber ;l++){
                   // emty array
                    $scope.data[vm.lineChartTitles[n]][l] = { values: [], key: $scope.sizes[l].name };
                    //canvas
                   if (!myLineChart[vm.lineChartTitles[n]].data.datasets[l]){
                        ///////////////todo: find a better way to do this
                        var colour_index = l;
                        if (colour_index > 19){
                            colour_index = l-20;
                        } else if (colour_index > 39){
                            colour_index = l-40;
                        }
                        myLineChart[vm.lineChartTitles[n]].data.datasets[l] = { data: [], backgroundColor: vm.default_colors[colour_index], borderColor: vm.default_colors[colour_index], label: $scope.sizes[l].name };
                    }

                    //get the array of the specific diagram of the specific cell or cloud
                    var array = vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name];
                   
                    // set data for the specific diagram of the specific cell or cloud
                    
                    var startTime = 0;
                    //if (vm.timelength > 200) startTime = vm.timelength - 200;
                    var endTime = vm.timelength -1;

                    if (num ==1){
                        endTime = Math.round(vm.timelength/3);
                    } else if (num ==2){
                        startTime = Math.round(vm.timelength/3) + 1;
                        endTime = Math.round((2*vm.timelength)/3);
                    } else if (num == 3) {
                        startTime = Math.round((2*vm.timelength)/3) + 1;
                    } else if (num == 4){
                        if (vm.timelength > 200) startTime = vm.timelength - 200;
                    }
                    for (startTime ; startTime < endTime +1; startTime++){
                        $scope.data[vm.lineChartTitles[n]][l].values.push({ x: array[startTime].t,	y: array[startTime].y});
                        // canvas/////////////////////////////////////////////////
                        if (l==0) myLineChart[vm.lineChartTitles[n]].data.labels.push(array[startTime].t);
                        myLineChart[vm.lineChartTitles[n]].data.datasets[l].data.push(array[startTime].y);
                    }

                     // set time
                    vm.time = numberWithCommas(array[endTime].t);
                    // set data of tidy tree chart
                    if (vm.lineChartTitles[n] == $scope.title){
                        var array2 = vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name];
                        if ($scope.sizes[l].name != 'Cloud'){
                            var child = search($scope.sizes[l].name,$scope.data3[0].children);
                            child.currentValue = array2[endTime].y;
                            child.currentSymbol = symbol[n];
                            child.currentIndex = endTime;
                            for (var grChild in child.children){
                                child.children[grChild].currentValue = child.children[grChild].treeMap[vm.lineChartTitles[n]][endTime].value;
                                child.children[grChild].currentSymbol = symbol[n];
                                child.children[grChild].currentIndex = endTime;
                            }
                        } else {
                            $scope.data3[0].currentValue = array2[endTime].y;
                            $scope.data3[0].currentSymbol = symbol[n];
                            $scope.data3[0].currentIndex = endTime;
                        }
                    }  
                }
            }
            /// canvas//////////
            myLineChart[vm.lineChartTitles[n]].update();
        }
        // update chart text
        TidyTreeService.updateNodeInfo(vm.svgTidy);
    }
    

    function updateCharts(){
            
            
            if (vm.x == vm.timelength) {
                $scope.run=false;
            } 
            if ($scope.run==false && vm.chartsEmpty == false){
                for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                    //canvas
                    myLineChart[vm.lineChartTitles[n]].options.pan.enabled = true;
                     myLineChart[vm.lineChartTitles[n]].options.zoom.enabled = true;
                } 
            } else {
                for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                    vm.chartsOptions[vm.lineChartTitles[n]].chart.zoom.enabled = false;

                    //canvas
                    myLineChart[vm.lineChartTitles[n]].options.pan.enabled = false;
                    myLineChart[vm.lineChartTitles[n]].options.zoom.enabled = false;
                   
                } 
            }
            if ($scope.run){
                vm.previousState = true;
            } else {
                vm.previousState = false;
            }
           
            //if ($scope.selectedSizes.length == 0) $scope.run=false;
            
            if (!$scope.run) return;
           
            // var linesNumber = $scope.selectedSizes.length;
            if (vm.x == 0){
                for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                    $scope.data[vm.lineChartTitles[n]] = [];
                    //////////////canvas
                    myLineChart[vm.lineChartTitles[n]].data.labels.length =0;
                    myLineChart[vm.lineChartTitles[n]].data.datasets.length =0;
                    //myLineChart[vm.lineChartTitles[n]].data = {labels: [], datasets: []};
                } 
                vm.chartsEmpty = false;           
            }
           
            //myLineChart.addData(Math.random() * 100, ++latestLabel);
            /////////////// take care!!!!!!!!!!!! vm.lineChartTitles2 ///////////////////////////////
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                if (vm.selectedTitles.indexOf(vm.lineChartTitles2[n]) != -1 || vm.lineChartTitles2[n] == $scope.title){
                
                var lineChartsNumber = $scope.sizes.length;
                for (var l=0 ; l< lineChartsNumber ;l++){
                    if (!$scope.data[vm.lineChartTitles[n]][l]){
                        $scope.data[vm.lineChartTitles[n]][l] = { values: [], key: $scope.sizes[l].name };
                    }
                    //canvas///////////////////////////////////////////////////////////////////////////////
                    
                    if (!myLineChart[vm.lineChartTitles[n]].data.datasets[l]){
                        ///////////////todo: find a better way to do this
                        var colour_index = l;
                        if (colour_index > 19){
                            colour_index = l-20;
                        } else if (colour_index > 39){
                            colour_index = l-40;
                        }
                        myLineChart[vm.lineChartTitles[n]].data.datasets[l] = { data: [], backgroundColor: vm.default_colors[colour_index], borderColor: vm.default_colors[colour_index], label: $scope.sizes[l].name };
                    }
                    //canvas/////////////////////////////////////////////////////////////////////////////////////////////////////
                    //get the array of the specific diagram of the specific cell or cloud
                    var array = vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name];

                    // set time
                    vm.time = numberWithCommas(array[vm.x].t);
                    

                    // canvas/////////////////////////////////////////////////
                    if (l==0) myLineChart[vm.lineChartTitles[n]].data.labels.push(array[vm.x].t);
                    myLineChart[vm.lineChartTitles[n]].data.datasets[l].data.push(array[vm.x].y);
                    

                    // shift values
                    //if ($scope.data[vm.lineChartTitles[n]][l].values.length > 100) $scope.data[vm.lineChartTitles[n]][l].values.shift();
                    // set data of tidy tree chart
                    if (vm.lineChartTitles2[n] == $scope.title){
                        var array2 = vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name];
                        if ($scope.sizes[l].name != 'Cloud'){
                            var child = search($scope.sizes[l].name,$scope.data3[0].children);
                            child.currentValue = array2[vm.x].y;
                            child.currentSymbol = symbol[n];
                            child.currentIndex = vm.x;
                            for (var grChild in child.children){
                                child.children[grChild].currentValue = child.children[grChild].treeMap[vm.lineChartTitles[n]][vm.x].value;
                                child.children[grChild].currentSymbol = symbol[n];
                                child.children[grChild].currentIndex = vm.x;
                            }
                        } else {
                            $scope.data3[0].currentValue = array2[vm.x].y;
                            $scope.data3[0].currentSymbol = symbol[n];
                            $scope.data3[0].currentIndex = vm.x;
                        }
                    }           
                }
              }
               /// canvas//////////
            myLineChart[vm.lineChartTitles[n]].update();
            }
            // update chart text
            TidyTreeService.updateNodeInfo(vm.svgTidy);

            // update bubble chart
            
            console.log('New bubble data');
            if (vm.myBubbleChart){
                vm.svgBubble = d3.select(".inner_bubble");
                // re-create raw data
                vm.bubbleChartData = [];
                createBubbleChartDataTimed(vm.x);
                console.log(vm.bubbleChartData);
                // re-create chart
                var bnames =[];
                for (var s=0; s< $scope.sizes.length-1; s++){
                    bnames.push($scope.sizes[s].name);
                }
                if (vm.x>1){
                    BubbleChartService.updateBubbles(vm.svgBubble, vm.bubbleChartData);
                } else {
                    vm.svgBubble.selectAll("*").remove();
                    vm.myBubbleChart = BubbleChartService.bubbleChart(bnames, BubbleTooltipService);
                    vm.myBubbleChart('.inner_bubble', vm.bubbleChartData);
                }
            
                
            }
            
            // increase x
            vm.x++;
        }

    function plot(x){
        if (x == 0){
            for (var n=0 ; n < vm.lineChartTitles.length ; n++){
                //////////////canvas
                myLineChart[vm.lineChartTitles[n]].data.labels.length =0;
                myLineChart[vm.lineChartTitles[n]].data.datasets.length =0;
            } 
            vm.chartsEmpty = false;           
        }


        /////////////// take care!!!!!!!!!!!! vm.lineChartTitles2 ///////////////////////////////
        for (var n=0 ; n < vm.lineChartTitles.length ; n++){
            if (vm.selectedTitles.indexOf(vm.lineChartTitles2[n]) != -1 || vm.lineChartTitles2[n] == $scope.title){
            
            var lineChartsNumber = $scope.sizes.length;
            for (var l=0 ; l< lineChartsNumber ;l++){
                
                //canvas///////////////////////////////////////////////////////////////////////////////
                
                if (!myLineChart[vm.lineChartTitles[n]].data.datasets[l]){
                    ///////////////todo: find a better way to do this
                    var colour_index = l;
                    if (colour_index > 19){
                        colour_index = l-20;
                    } else if (colour_index > 39){
                        colour_index = l-40;
                    }
                    myLineChart[vm.lineChartTitles[n]].data.datasets[l] = { data: [], backgroundColor: vm.default_colors[colour_index], borderColor: vm.default_colors[colour_index], label: $scope.sizes[l].name };
                }
                //canvas/////////////////////////////////////////////////////////////////////////////////////////////////////
                //get the array of the specific diagram of the specific cell or cloud
                var array = vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name];
                

                // set time
                //console.log('x=' + x + '\n') ;
                vm.time = numberWithCommas(array[x].t);
                
                

                // canvas/////////////////////////////////////////////////
                if (l==0) myLineChart[vm.lineChartTitles[n]].data.labels.push(array[x].t);
                myLineChart[vm.lineChartTitles[n]].data.datasets[l].data.push(array[x].y);
                

                
                //set data of tidy tree chart
                if (vm.lineChartTitles2[n] == $scope.title){
                    //console.log('Tidy tree\n');
                    var array2 = vm.chartMaps[vm.lineChartTitles[n]][$scope.sizes[l].name];
                    if ($scope.sizes[l].name != 'Cloud'){
                        var child = search($scope.sizes[l].name,$scope.data3[0].children);
                        child.currentValue = array2[x].y;
                        child.currentSymbol = symbol[n];
                        child.currentIndex = x;
                        
                        for (var grChild in child.children){
                            child.children[grChild].currentValue = child.children[grChild].treeMap[vm.lineChartTitles[n]][x].value;
                            child.children[grChild].currentSymbol = symbol[n];
                            child.children[grChild].currentIndex = x;
                        }
                    } else {
                        $scope.data3[0].currentValue = array2[x].y;
                        $scope.data3[0].currentSymbol = symbol[n];
                        $scope.data3[0].currentIndex = x;
                    }
                }           
            }
          }
           /// canvas//////////
            myLineChart[vm.lineChartTitles[n]].update();
        }
        // update chart text
        console.log('Time:'+ vm.time + '\n');
        console.log('Update node info');
        TidyTreeService.updateNodeInfo(vm.svgTidy);

        // update bubble info
        console.log('New bubble data');
        if (vm.myBubbleChart){
            vm.svgBubble = d3.select(".inner_bubble");
            // re-create raw data
            vm.bubbleChartData = [];
            createBubbleChartDataTimed(x);
            console.log(vm.bubbleChartData);
            // re-create chart
            var bnames =[];
            for (var s=0; s< $scope.sizes.length-1; s++){
                bnames.push($scope.sizes[s].name);
            }
            if (x>1){
                BubbleChartService.updateBubbles(vm.svgBubble, vm.bubbleChartData);
            } else {
                vm.svgBubble.selectAll("*").remove();
                vm.myBubbleChart = BubbleChartService.bubbleChart(bnames, BubbleTooltipService);
                vm.myBubbleChart('.inner_bubble', vm.bubbleChartData);
            }
           
            
        }
    }

    function initialize(){
        // initialize graph data
        // set all the line charts options (title, axis etc.)
        for (var n=0 ; n < vm.lineChartTitles.length ; n++){
            $scope.data[vm.lineChartTitles[n]] = [];
        }
        for (i=0;i<vm.generalInfo.length;i++){
            vm.generalInfo[i].value = null;
        }
         
        vm.chartsEmpty = true;     
        
        
        // tree data
        vm.treeMap = jsonInfo.treeMapInit(vm.lineChartTitles);
        $scope.data3 = [{name: "Cloud",
                        parent: null,
                        treeMap: vm.treeMap,   
                        processorUtilization: [],
                        children:[],
                        currentValue: null,
                        currentSymbol: "",
                        currentIndex : null
                    }];
        console.log('Inside initialize');
        console.log($scope.data3);
        $scope.sizes = [];
        vm.timelength =0;
        vm.chartMaps = {};
        
        vm.x =0;
        
        if (vm.svgTidy){
            vm.svgTidy = d3.select("section.tidy");
            vm.svgTidy.selectAll("*").remove();
        }
        if (vm.myBubbleChart){
            vm.svgBubble = d3.select(".inner_bubble");
            vm.svgBubble.selectAll("*").remove();
        }
         

    }
    // search element by name inside an array
    function search(nameKey, myArray){
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].name === nameKey) {
                return myArray[i];
            }
        }
    }

    function createBubbleChartDataTimed(x){
        //bubble chart data
        var nameIds = vm.chartObject.nameIds;
        console.log('utilizationHWTypeTimed');
        console.log(vm.chartObject.utilizationHWTypeTimed);
        for (var id=0; id<nameIds.length;id++){
            var d = vm.chartObject.utilizationHWTypeTimed[$scope.selectedUtilizationName][nameIds[id]][x];
            d.id =id;
           vm.bubbleChartData.push(vm.chartObject.utilizationHWTypeTimed[$scope.selectedUtilizationName][nameIds[id]][x]);
        }
    }

    function createBubbleChartData(){
        //bubble chart data
        var nameIds = vm.chartObject.nameIds;
        
        for (var id=0; id<nameIds.length;id++){
            var d = vm.chartObject.utilizationHWTypeAverage[$scope.selectedUtilizationName][nameIds[id]];
            d.id =id;
           vm. bubbleChartData.push(vm.chartObject.utilizationHWTypeAverage[$scope.selectedUtilizationName][nameIds[id]]);
        }
    }

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    function generateTidyTree(names){
         // ************** Generate the tree diagram	 *****************
         var diameter = 860;
         var radius = 10;
         var strock_width = 3;
         var t_size = 12;
         var depth = 100;
         /// i should find a better way
         if (names.length > 10){
             diameter = 960;
             depth = 120;
         }
         if (names.length > 20){
             radius = 5;
             strock_width = 2;
             t_size = 11;
         }
         if (names.length > 30){
             radius = 3;
             strock_width = 1.5;
             t_size = 9;
         }
         if (names.length > 50){
             radius = 2;
             strock_width = 1;
             t_size = 8;
         }
         //diameter = $(document).width();
         var padding = 10;
         
             
         var i = 0;

        
         var tree = d3.layout.tree()
             .size([360, diameter/2 - padding]);
             //.separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });


         // var diagonal = d3.svg.diagonal()
         //     .projection(function(d) { return [d.y, d.x]; });
         var diagonal = d3.svg.diagonal.radial()
             .projection(function(d) { 
                 return [1.5 *d.y, d.x / 180 * Math.PI]; 
             });

         vm.svgTidy = d3.select("section.tidy").append("svg")
             .attr("width", diameter)
             .attr("height", diameter)
             .append("g")
             .attr("transform", "translate(" + diameter/2 + "," + diameter/2 + ")");
             
             
     
         // load data
         TidyTreeService.update(i, tree, diagonal, vm.svgTidy, $scope.data3, BubbleTooltipService, vm.lineChartTitles, symbol, radius, strock_width, t_size, depth);

    }
      
    }
})();