(function(){
  'use strict';

  angular.module('app')
    .factory('jsonInfo', [
        '$http',
        jsonInfo
    ]);

    function jsonInfo($http) {
        
        return {   
            getRecords: getRecords,
            getChartMaps : getChartMaps,
            treeMapInit : treeMapInit     
        };
        // this function was used to get the json file locally - it not used any more
        function getRecords() {
            return $http.get('/testdist/simulator/outputs/outputCLSim.json');  
        }

        // this function takes the json input and creates the arrays for each measured value
        // then in the controller these arrays are used to update interval
        function getChartMaps(names, obj, data, lineChartTitles){
            var chartMaps = {};
            for (var n=0 ; n < lineChartTitles.length ; n++){
                chartMaps[lineChartTitles[n]] = {};
            }
            //arrays for cloud sums
            var arrayNames = ['Total Energy Consumption', 'Active Servers', 'Total Number of accepted Tasks', 'Total Number of rejected Tasks', 'Running VMs'];
            var sumCloudArrays = {};
            for (var s=0 ; s < arrayNames.length ; s++){
                sumCloudArrays[arrayNames[s]] = [];
            }
            // arrays for cloud sums of utilized
            var arrayUtilizedNames = ['Actual Utilized Processors1', 'Actual Utilized Processors2', 'Actual Utilized Memory1', 'Actual Utilized Memory2',
            'Actual Utilized Network', 'Utilized Storage1', 'Utilized Storage2', 'Utilized Accelerators1', 'Utilized Accelerators2'];
            var sumCloudUtilizedArrays = {};
            for (var s=0 ; s < arrayUtilizedNames.length ; s++){
                sumCloudUtilizedArrays[arrayUtilizedNames[s]] = [];
            }
            // arrays for cloud utilization and average utilization
            var utilizationArrayNames = ['Processor Utilization over Active Servers', 'Processor Utilization', 'Memory Utilization over Active Servers', 'Memory Utilization',
            'Network Utilization', 'Storage Utilization over Active Servers', 'Storage Utilization', 'Accelerator Utilization over Active Servers', 'Accelerator Utilization'];
            var utilizationCloudArrays = {};
            var utilizationCloudAverage = {};
            var utilizationHWTypeAverage = {};
            var utilizationHWTypeTimed = {};
            for (var u=0 ; u < utilizationArrayNames.length ; u++){
                utilizationCloudArrays[utilizationArrayNames[u]] = [];
                utilizationCloudAverage[utilizationArrayNames[u]] = 0.0;
                utilizationHWTypeAverage[utilizationArrayNames[u]] = {};
                utilizationHWTypeTimed[utilizationArrayNames[u]] = {};
            }
            

            // sums for totals in cloud
            var totalsNames = ['Total Processors over Active Servers', 'Total Processors', 'Total Memory over Active Servers','Total Memory', 'Total Network',
            'Total Storage over Active Servers', 'Total Storage', 'Total Accelerators over Active Servers','Total Accelerators'];
            var sumCloudTotalsArrays = {};
             for (var s=0 ; s < totalsNames.length ; s++){
                sumCloudTotalsArrays[totalsNames[s]] = [];
            }

            
            var nameIds = [];
            var timelength = 0;
            var data3 = data;
            

               for (var i=0;i<names.length;i++){
                   var strName = names[i];
                  
                   if (obj[strName]){
                      // cell arrays for sums
                      var sumCellArrays = {};
                      for (var s=0 ; s < arrayNames.length ; s++){
                            sumCellArrays[arrayNames[s]] = [];
                      }
                      // cell arrays for sums of utilized values
                      var sumCellUtilizedArrays = {};
                      for (var s=0 ; s < arrayUtilizedNames.length ; s++){
                            sumCellUtilizedArrays[arrayUtilizedNames[s]] = [];
                      }

                      var sumCellTotalsArrays = {};
                      for (var s=0 ; s < totalsNames.length ; s++){
                            sumCellTotalsArrays[totalsNames[s]] = [];
                      }


                     // arrays for cell utilization
                    var utilizationCellArrays = {};
                    for (var u=0 ; u < utilizationArrayNames.length ; u++){
                        utilizationCellArrays[utilizationArrayNames[u]] = [];
                    }
                    

                       // add cell children to tidy tree
                       data3[0].children[i] = {name: strName, parent: "Cloud", 
                       treeMap: {}, 
                       processorUtilization: [], children: [], currentValue: null, currentIndex: null};
                       
                       data3[0].children[i].treeMap = treeMapInit(lineChartTitles);
                        
                       // I take the time length from the first element but it should be the same for each element
                       timelength = obj[strName][0]["Outputs"].length;                   
                       // for each time step count energy and utilization
                       for (var j=0; j< timelength; j++){
                           // step sum
                           var sumStep = {};
                           for (var s=0 ; s < arrayNames.length ; s++){
                                sumStep[arrayNames[s]] = 0.0;
                            }
                            // step for sums of utilized values
                            var sumUtilizedStep = {};
                            for (var s=0 ; s < arrayUtilizedNames.length ; s++){
                                sumUtilizedStep[arrayUtilizedNames[s]] = 0.0;
                            }
                        

                          // sums for totals in cell (e.g. totalProcessors)
                            var sumTotalsStep = {};
                            for (var s=0 ; s < totalsNames.length ; s++){
                                sumTotalsStep[totalsNames[s]] = 0.0;
                            }
                           
                            // for each element in the cell (each hwType of the cell)
                          for (var k=0; k< obj[strName].length; k++){
                              // for each measurement value e.g energy consumption we calculate the sum of values and the sum of utilized values
                              // also we calculate the value of each hwType for this specific time step
                              var hwTypeValues = {};
                              for (var s=0 ; s < arrayNames.length ; s++){
                                sumStep[arrayNames[s]] += obj[strName][k]["Outputs"][j][arrayNames[s]];
                                if (arrayNames[s] == 'Total Energy Consumption') {
                                    hwTypeValues[arrayNames[s]] = obj[strName][k]["Outputs"][j][arrayNames[s]] * Math.pow(10, 3);
                                } else {
                                    hwTypeValues[arrayNames[s]] = obj[strName][k]["Outputs"][j][arrayNames[s]];
                                }
                                
                            }
                             for (var s=0 ; s < arrayUtilizedNames.length ; s++){
                                sumUtilizedStep[arrayUtilizedNames[s]] += obj[strName][k]["Outputs"][j][arrayUtilizedNames[s].replace(/[0-9]/g, '')];
                                sumTotalsStep[totalsNames[s]] += obj[strName][k]["Outputs"][j][totalsNames[s]];
                                var utilized = obj[strName][k]["Outputs"][j][arrayUtilizedNames[s].replace(/[0-9]/g, '')];
                                var totalOverActive =  obj[strName][k]["Outputs"][j][totalsNames[s]];
                                if (utilized==0 && totalOverActive==0){
                                    hwTypeValues[utilizationArrayNames[s]] = 0;
                                } else {
                                    hwTypeValues[utilizationArrayNames[s]] = (utilized / totalOverActive) * 100;
                                }
                                
                            }
                        
                           // add hw types to tidy tree

                           //var hwTypeUtilization = (obj[strName][k]["Outputs"][j]["Actual Utilized Processors"]) / (obj[strName][k]["Outputs"][j]["Total Processors"]);
                            var hwType = '';
                            var hwTypeNum = obj[strName][k]["HW Type"];
                            if (hwTypeNum == 1){
                                hwType = 'CPU';
                            } else if (hwTypeNum == 2){
                                    hwType = 'GPU';
                            } else if (hwTypeNum == 3){
                                hwType = 'DFE';
                            } else if (hwTypeNum == 4){
                                    hwType = 'MIC';
                            }
                           if (!data3[0].children[i].children[k]){
                                
                                data3[0].children[i].children[k];
                                data3[0].children[i].children[k] = {name: hwType, parent: strName, treeMap: {}, processorUtilization: [], children: [], currentValue:null, currentIndex: null};
                                data3[0].children[i].children[k].treeMap = treeMapInit(lineChartTitles);
                           }
                           var nameId = strName + ":" + hwType;
                           if (!utilizationHWTypeAverage["Processor Utilization"][nameId]){
                               nameIds.push(nameId);
                           }
                           for (var u=0 ; u < utilizationArrayNames.length ; u++){
                            if (!utilizationHWTypeAverage[utilizationArrayNames[u]][nameId]){
                                utilizationHWTypeAverage[utilizationArrayNames[u]][nameId];
                                utilizationHWTypeAverage[utilizationArrayNames[u]][nameId] = {name:nameId, group:hwType, cell:strName, cellNum:strName.split('-').pop(), id:hwTypeNum, total_amount: hwTypeValues[utilizationArrayNames[u]], measuredVanue:utilizationArrayNames[u]};
                            } else {
                                utilizationHWTypeAverage[utilizationArrayNames[u]][nameId].total_amount += hwTypeValues[utilizationArrayNames[u]];
                            }
                            //timed
                            if (!utilizationHWTypeTimed[utilizationArrayNames[u]][nameId]){
                                utilizationHWTypeTimed[utilizationArrayNames[u]][nameId]=[];
                            }
                            utilizationHWTypeTimed[utilizationArrayNames[u]][nameId].push({name:nameId, group:hwType, cell:strName, cellNum:strName.split('-').pop(), id:hwTypeNum, total_amount: hwTypeValues[utilizationArrayNames[u]], measuredVanue:utilizationArrayNames[u]});
                           }
                           for (var n=0 ; n < lineChartTitles.length ; n++){
                             data3[0].children[i].children[k].treeMap[lineChartTitles[n]].push({t:obj[strName][k]["Outputs"][j]["Time Step"], value: hwTypeValues[lineChartTitles[n]]});
                           }
                           
                          }
                          // fill arrays for each cell
                        var timeStep = obj[strName][0]["Outputs"][j]["Time Step"];
                        
                         for (var s=0 ; s < arrayNames.length ; s++){
                             if (arrayNames[s] == 'Total Energy Consumption') {
                                 sumCellArrays[arrayNames[s]].push({t:timeStep, y: sumStep[arrayNames[s]] * Math.pow(10, 3)});
                             } else {
                                sumCellArrays[arrayNames[s]].push({t:timeStep, y: sumStep[arrayNames[s]]});
                             }
                         }
                         for (var s=0 ; s < arrayUtilizedNames.length ; s++){
                             sumCellUtilizedArrays[arrayUtilizedNames[s]].push({t:(timeStep), y: sumUtilizedStep[arrayUtilizedNames[s]]});
                         }
                         for (var s=0 ; s < totalsNames.length ; s++){
                             sumCellTotalsArrays[totalsNames[s]].push({t:(timeStep), y: sumTotalsStep[totalsNames[s]]});
                         }
                        

                        // util
                        for (var u=0 ; u < utilizationArrayNames.length ; u++){
                            var stepUtilization = sumCellUtilizedArrays[arrayUtilizedNames[u]][j].y / sumCellTotalsArrays[totalsNames[u]][j].y;
                            utilizationCellArrays[utilizationArrayNames[u]].push({t:(timeStep), y: stepUtilization * 100});
                        }
                        
                        
                        
                        // tidy tree
                        for (var n=0 ; n < lineChartTitles.length ; n++){
                            if (sumCellArrays[lineChartTitles[n]]){
                             data3[0].children[i].treeMap[lineChartTitles[n]].push({t:(timeStep), value: sumCellArrays[lineChartTitles[n]][j].y});
                            } else {
                             data3[0].children[i].treeMap[lineChartTitles[n]].push({t:(timeStep), value: utilizationCellArrays[lineChartTitles[n]][j].y});
                            }
                        }
                       }
                       

                       console.log(strName + ":\n"); 
                       for (var t=0; t< timelength;t++){
                           // fill arrays for cloud sums by adding the cell values
                           for (var s=0 ; s < arrayNames.length ; s++){
                            fillCloudArrays(sumCloudArrays[arrayNames[s]], sumCellArrays[arrayNames[s]], t);
                           }
                           for (var s=0 ; s < arrayUtilizedNames.length ; s++){
                                fillCloudArrays(sumCloudUtilizedArrays[arrayUtilizedNames[s]], sumCellUtilizedArrays[arrayUtilizedNames[s]], t);
                            }
                            for (var s=0 ; s < totalsNames.length ; s++){
                                fillCloudArrays(sumCloudTotalsArrays[totalsNames[s]], sumCellTotalsArrays[totalsNames[s]], t);
                            }
                            
                       
                            
                       }
                       // pass cells to chartmap
                       for (var n=0 ; n < lineChartTitles.length ; n++){
                           if (sumCellArrays[lineChartTitles[n]]){
                              chartMaps[lineChartTitles[n]][strName] = sumCellArrays[lineChartTitles[n]] ;
                           } else if (utilizationCellArrays[lineChartTitles[n]]){
                               chartMaps[lineChartTitles[n]][strName] = utilizationCellArrays[lineChartTitles[n]] ;
                           }
                        }
                   
                   }  
               }
               // cloud utilization arrays
               for (var t=0; t< timelength;t++){
                   for (var u=0 ; u < utilizationArrayNames.length ; u++){
                       if (sumCloudTotalsArrays[totalsNames[u]][t].y==0) sumCloudTotalsArrays[totalsNames[u]][t].y=1;
                        utilizationCloudArrays[utilizationArrayNames[u]].push({t: sumCloudUtilizedArrays[arrayUtilizedNames[u]][t].t, y:(sumCloudUtilizedArrays[arrayUtilizedNames[u]][t].y/sumCloudTotalsArrays[totalsNames[u]][t].y)*100});
                        
                    }
                    // tidy tree
                    for (var n=0 ; n < lineChartTitles.length ; n++){
                        if (sumCloudArrays[lineChartTitles[n]]){
                        data3[0].treeMap[lineChartTitles[n]].push({t:sumCloudArrays[lineChartTitles[n]][t].t, value: sumCloudArrays[lineChartTitles[n]][t].y});
                        } else {
                        data3[0].treeMap[lineChartTitles[n]].push({t:utilizationCloudArrays[lineChartTitles[n]][t].t, value: utilizationCloudArrays[lineChartTitles[n]][t].y});
                        }
                    }
               }
               // utilization average
                for (var u=0 ; u < utilizationArrayNames.length ; u++){
                    for (var t=0; t< timelength;t++){
                        utilizationCloudAverage[utilizationArrayNames[u]] += utilizationCloudArrays[utilizationArrayNames[u]][t].y;
                    }
                    utilizationCloudAverage[utilizationArrayNames[u]] = utilizationCloudAverage[utilizationArrayNames[u]] / timelength;
                    for (var n=0; n<nameIds.length; n++){
                        utilizationHWTypeAverage[utilizationArrayNames[u]][nameIds[n]].total_amount /= timelength;
                    }
                }
               // pass cloud arrays to chartmap
               for (var n=0 ; n < lineChartTitles.length ; n++){
                    if (sumCloudArrays[lineChartTitles[n]]){
                        chartMaps[lineChartTitles[n]]["Cloud"] = sumCloudArrays[lineChartTitles[n]] ;
                    } else if (utilizationCloudArrays[lineChartTitles[n]]){
                        chartMaps[lineChartTitles[n]]["Cloud"] = utilizationCloudArrays[lineChartTitles[n]] ;
                    }
                }
                

                 return {   
            chartMaps : chartMaps,
            data3 : data3,
            timelength : timelength,
            utilizationCloudAverage : utilizationCloudAverage,
            nameIds : nameIds,
            utilizationHWTypeAverage : utilizationHWTypeAverage,
            utilizationHWTypeTimed: utilizationHWTypeTimed      
        };

    }
    
    function fillCloudArrays(cloudArray, cellArray, t){
        if (cloudArray[t]){
            cloudArray[t].y += cellArray[t].y;
        } else {
            cloudArray.push({t: cellArray[t].t, y:cellArray[t].y});
        }
    }

    function treeMapInit(lineChartTitles) {
        var treeMapInit = {};
        for (var n=0 ; n < lineChartTitles.length ; n++){
            treeMapInit[lineChartTitles[n]] = [];
        } 
        return treeMapInit;
    }

    }

})();
