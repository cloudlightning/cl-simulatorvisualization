(function () {
    'use strict';

    angular.module('app')
        .service('ChartsService', [
            ChartsService
        ]);

    function ChartsService() {

        var service = {
            getTestEnergyConsumptionData: getTestEnergyConsumptionData,
            chartOptions : chartOptions,
            canvasChartOptions: canvasChartOptions
        };

        return service;

        
    }

    function getTestEnergyConsumptionData() {
            
            var table = [];
            for (var i = 0; i < 100; i++) {
                table.push({x: i, y: i*2});
            }
            return [ { values: table, key: 'Energy Consumption', color: 'rgb(0, 150, 136)', area: true  } ];
        }

    function chartOptions(c_title, x_label, t_format, speed){
        return {
            chart: {
                type: 'lineChart',
                height: 300,
                margin: { left: 100, right: 100 },
                x: function (d) { return d.x },
                y: function (d) { return d.y },
                showLabels: true,
                showLegend: true,
                title: c_title,
                xAxis: {
                    axisLabel: 'Time (s)'
                },
                useInteractiveGuideline: true,
                duration: speed,    
                yAxis: {
                    axisLabel: x_label,
                    tickFormat: function(d){
                   return d3.format(t_format)(d);
                    }
                },
                forceY: 0, 
                // forceX: 0,   
                showYAxis: true,
                showXAxis: true,
                zoom: {
                    enabled: false,
                    scaleExtent: [
                        1,
                        10
                    ],
                    useFixedDomain: false,
                    useNiceScale: false,
                    horizontalOff: false,
                    verticalOff: false,
                    unzoomEventType: "dblclick.zoom"
                }
                 //refreshDataOnly: true
                // tooltip: { contentGenerator: function (d) { return '<span class="custom-tooltip">' + Math.round(d.point.y) + '</span>' } }
            },
            title: {
                enable: true,
                text: c_title + " Chart",
                css: {
                    'text-align': 'left',
                    'color': 'black',
                    'padding-top': '20px',
                    'padding-left': '10px',
                    'font': '14px sans-serif',
                    'text-decoration': 'underline'
                }
            }
            
        }
        
    }

    function canvasChartOptions(c_title, y_label){
        return {
            elements:{ 
                line: {
                    fill:false,
                    tension: 0,
                    borderWidth: 2
                },
                point: {
                    hoverRadius: 4,
                    hoverBorderWidth:2,
                    radius:1
                }
            },
            showLines: true,
            tooltips: {
                mode: 'index'
            },
            layout: {
                padding: {
                right: 30
                }
            },
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Time (s)'
                    },
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 20,
                        beginAtZero: true,
                        callback: function(label, index, labels) {
                            return label.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: y_label
                    },
                    ticks: {
                        padding: 10,
                        beginAtZero:true,
                        callback: function(label, index, labels) {
                            if (label<1 && label>0.00099){
                                var label3 =  Math.floor(label * 1000) / 1000;
                                return label3;
                            }else if(label<0.00099 && label!=0){
                                return label.toExponential(1);
                            }
                            var label2 =  Math.floor(label * 10) / 10;
                            var parts = label2.toString().split(".");
                            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            return parts.join(".");
                        }
                    }
                    }]
            },
            title: {
                display: true,
                text: c_title + " Chart"
            },
           
            //Container for pan options
            pan: {
                // Boolean to enable panning
                enabled: false,

                // Panning directions. Remove the appropriate direction to disable 
                // Eg. 'y' would only allow panning in the y direction
                mode: 'xy',
                speed: 50,
                threshold: 10
            },

            // Container for zoom options
            zoom: {
                // Boolean to enable zooming
                enabled: false,
                drag: true,
                // Zooming directions. Remove the appropriate direction to disable 
                // Eg. 'y' would only allow zooming in the y direction
                mode: 'xy',
                limits: {
                    max: 10,
                    min: 10
                }
            }
        }

    }

    
})();