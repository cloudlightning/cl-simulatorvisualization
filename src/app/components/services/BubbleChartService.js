(function () {
    'use strict';

    angular.module('app')
        .service('BubbleChartService', [
            BubbleChartService
        ]);

    function BubbleChartService() {

        var service = {
            bubbleChart: bubbleChart,
            //bubbleChart2: bubbleChart2,
            updateBubbles:updateBubbles
            //setupButtons : setupButtons
        };

        return service;

        
    }

    /* bubbleChart creation function. Returns a function that will
 * instantiate a new bubble chart given a DOM element to display
 * it in and a dataset to visualize.
 *
 * Organization and style inspired by:
 * https://bost.ocks.org/mike/chart/
 *
 */
var maxRange = 100;
// Constants for sizing
var width = 2000;
var height = 850;
var marginText = 60;
var marginCellCenters = 400;

// Charge function that is called for each node.
  // Charge is proportional to the diameter of the
  // circle (which is stored in the radius attribute
  // of the circle's associated data.
  // This is done to allow for accurate collision
  // detection with nodes of different sizes.
  // Charge is negative because we want nodes to repel.
  // Dividing by 8 scales down the charge to be
  // appropriate for the visualization dimensions.
  function charge(d) {
    return -Math.pow(d.radius, 2.0) / 8;
  }

function bubbleChart(cellNames, BubbleTooltipService) {
  

  // TODO: tooltip for mouseover functionality
  var tooltip = BubbleTooltipService.floatingTooltip('bubble_tooltip', 40);

  // Locations to move bubbles towards, depending
  // on which view mode is selected.
  var center = { x: (width) / 2, y: height / 2 };
  var cellCenters = {};
  var cellsTitleX = {};
   
  if (cellNames.length ==1){
    cellCenters[cellNames[0]] = {x: (width/ 2), y: height / 2};
    cellsTitleX[cellNames[0]] = (width/ 2);
  } else if (cellNames.length>1 && cellNames.length<=15){
    var cellTextLength = (width-marginText) / cellNames.length;
    var cellLength = (width-marginCellCenters) / cellNames.length;

    for (var c=0 ; c<cellNames.length ; c++){
        cellCenters[cellNames[c]] = {x: 200 +(c+1)*cellLength - (cellLength/2) , y: height / 2};
        cellsTitleX[cellNames[c]] = (c+1)*cellTextLength - (cellTextLength/2);
        
    }
  } else {
    var length = Math.floor(cellNames.length/10);
    var cellTextLength = (width-marginText) / length;
    var cellLength = (width-marginCellCenters) / length;
    var map = {};
    for (var c=0 ; c<cellNames.length ; c++){ 
      
            if (c<10){
              map[c] = 0;
            } else if (c>=10 && c< 20){
              map[c] = 1;
            }else if (c>=20 && c< 30){
              map[c] = 2;
            }else if (c>=30 && c< 40){
              map[c] = 3;
            }else if (c>=40 && c< 50){
              map[c] = 4;
            }else if (c>=50 && c< 60){
              map[c] = 5;
            }else if (c>=60 && c< 70){
              map[c] = 6;
            }else if (c>=70 && c< 80){
              map[c] = 7;
            }else if (c>=80 && c< 90){
              map[c] = 8;
            }else if (c>=90 && c< 100){
              map[c] = 9;
            }
        
    }

    for (var c=0 ; c<cellNames.length ; c++){     
        cellCenters[cellNames[c]] = {x: 200 +(map[c]+1)*cellLength - (cellLength/2) , y: height / 2};
        cellsTitleX[cellNames[c]] = (map[c]+1)*cellTextLength - (cellTextLength/2);
    }
  }


  // Used when setting up force and
  // moving around nodes
  var damper = 0.102;

  // These will be set in create_nodes and create_vis
  var svg = null;
  var bubbles = null;
  var nodes = [];

  

  // Here we create a force layout and
  // configure it to use the charge function
  // from above. This also sets some contants
  // to specify how the force layout should behave.
  // More configuration is done below.
  var force = d3.layout.force()
    .size([width, height])
    .charge(charge)
    .gravity(-0.01)
    .friction(0.85);


  // Nice looking colors - no reason to buck the trend
  var fillColor = d3.scale.ordinal()
    .domain(['CPU', 'GPU', 'MIC', 'DFE'])
    .range(['#d84b2a', '#beccae', '#7aa25c', '#AC58FA']);

  // Sizes bubbles based on their area instead of raw radius
  // hardcoded!
  //var maxRange = 100;
  if (cellNames.length > 5){
    maxRange = 55;
  }
  if (cellNames.length > 10){
    maxRange = 40;
  }
  if (cellNames.length > 15){
    maxRange = 20;
  }
  

    var radiusScale = d3.scale.pow()
    .exponent(0.6)
    .range([2, maxRange]);

  /*
   * This data manipulation function takes the raw data from
   * the CSV file and converts it into an array of node objects.
   * Each node will store data and visualization values to visualize
   * a bubble.
   *
   * rawData is expected to be an array of data objects, read in from
   * one of d3's loading functions like d3.csv.
   *
   * This function returns the new node array, with a node in that
   * array for each element in the rawData input.
   */
  function createNodes(rawData) {
    // Use map() to convert raw data into node data.
    // Checkout http://learnjsdata.com/ for more on
    // working with data.
    var myNodes = rawData.map(function (d) {
      return {
        id: d.id,
        radius: radiusScale(+d.total_amount),
        value: d.total_amount,
        name: d.name,
        measuredVanue: d.measuredVanue,
        //name: d.grant_title,
        // org: d.organization,
         group: d.group,
         cell : d.cell,
         cellNum : d.cellNum,
        // year: d.start_year,
        x: Math.random() * 900,
        y: Math.random() * 800
      };
    });

    // sort them to prevent occlusion of smaller nodes.
    myNodes.sort(function (a, b) { return b.value - a.value; });

    return myNodes;
  }

  /*
   * Main entry point to the bubble chart. This function is returned
   * by the parent closure. It prepares the rawData for visualization
   * and adds an svg element to the provided selector and starts the
   * visualization creation process.
   *
   * selector is expected to be a DOM element or CSS selector that
   * points to the parent element of the bubble chart. Inside this
   * element, the code will add the SVG continer for the visualization.
   *
   * rawData is expected to be an array of data objects as provided by
   * a d3 loading function like d3.csv.
   */
  var chart = function chart(selector, rawData) {
    // Use the max total_amount in the data as the max in the scale's domain
    // note we have to ensure the total_amount is a number by converting it
    // with `+`.
    var maxAmount = d3.max(rawData, function (d) { return +d.total_amount; });
    radiusScale.domain([0, maxAmount]);

    nodes = createNodes(rawData);
    // Set the force's nodes to our newly created nodes array.
    force.nodes(nodes);

    // Create a SVG element inside the provided selector
    // with desired size.
    svg = d3.select(selector)
      .append('svg')
      .attr('width', width)
      .attr('height', height);

    // Bind nodes data to what will become DOM elements to represent them.
    bubbles = svg.selectAll('inner_bubble')
      .data(nodes, function (d) { return d.id; });

    // Create new circle elements each with class `bubble`.
    // There will be one circle.bubble for each object in the nodes array.
    // Initially, their radius (r attribute) will be 0.
    bubbles.enter().append('circle')
      .classed('bubble', true)
      .attr('r', 0)
      .attr('fill', function (d) { return fillColor(d.group); })
      .attr('stroke', function (d) { return d3.rgb(fillColor(d.group)).darker(); })
      .attr('stroke-width', 2)
       .on('mouseover', showDetail)
       .on('mouseout', hideDetail)
      ;

    // Fancy transition to make bubbles appear, ending with the
    // correct radius
    bubbles.transition()
      .duration(2000)
      .attr('r', function (d) {return d.radius; });

    // Set initial layout to single group.
    groupBubbles();

    /// legend
    var legendNames =['CPU servers', 'CPU + GPU pairs', 'CPU + MIC pairs', 'CPU + DFE pairs'];
    var legendColours =['#d84b2a', '#beccae', '#7aa25c', '#AC58FA'];

    var color_legend = svg.append("g")
	                    .attr("class", "legend")
	                    .attr("transform", "translate(" + (50) + "," + (750) + ")")
	                    .selectAll("g")
	                    .data(legendNames)
	                    .enter().append("g");
  
    color_legend.append("circle")
			      .attr("cy", function(d,i) { return i*20; })
                  .attr("r", 8)
				  .style("fill", function(d,i) {
							return legendColours[i];
						 })
				  .attr("opacity", 1);
   color_legend.append("text")
        .attr("y", function(d,i) { return i*20; })
		.attr("dy", 4)
		.attr("x", 40)		  
        .text(function(d,i) {
        	return d
        });
  };



  /*
   * Sets visualization in "single group mode".
   * The cell labels are hidden and the force layout
   * tick function is set to move all nodes to the
   * center of the visualization.
   */
  function groupBubbles() {
    hidecells();

    force.on('tick', function (e) {
      bubbles.each(moveToCenter(e.alpha))
        .attr('cx', function (d) { return d.x; })
        .attr('cy', function (d) { return d.y; });
    });

    force.start();
  }

  /*
   * Helper function for "single group mode".
   * Returns a function that takes the data for a
   * single node and adjusts the position values
   * of that node to move it toward the center of
   * the visualization.
   *
   * Positioning is adjusted by the force layout's
   * alpha parameter which gets smaller and smaller as
   * the force layout runs. This makes the impact of
   * this moving get reduced as each node gets closer to
   * its destination, and so allows other forces like the
   * node's charge force to also impact final location.
   */
  function moveToCenter(alpha) {
    return function (d) {
      d.x = d.x + (center.x - d.x) * damper * alpha;
      d.y = d.y + (center.y - d.y) * damper * alpha;
    };
  }

  /*
   * Sets visualization in "split by cell mode".
   * The cell labels are shown and the force layout
   * tick function is set to move nodes to the
   * cellCenter of their data's cell.
   */
  function splitBubbles() {
    showcells();

    force.on('tick', function (e) {
      bubbles.each(moveTocells(e.alpha))
        .attr('cx', function (d) { return d.x; })
        .attr('cy', function (d) { return d.y; });
    });

    force.start();
  }

  /*
   * Helper function for "split by cell mode".
   * Returns a function that takes the data for a
   * single node and adjusts the position values
   * of that node to move it the cell center for that
   * node.
   *
   * Positioning is adjusted by the force layout's
   * alpha parameter which gets smaller and smaller as
   * the force layout runs. This makes the impact of
   * this moving get reduced as each node gets closer to
   * its destination, and so allows other forces like the
   * node's charge force to also impact final location.
   */
  function moveTocells(alpha) {
    return function (d) {
      var target = cellCenters[d.cell];
      d.x = d.x + (target.x - d.x) * damper * alpha * 1.1;
      d.y = d.y + (target.y - d.y) * damper * alpha * 1.1;
    };
  }

  /*
   * Hides cell title displays.
   */
  function hidecells() {
    svg.selectAll('.cell').remove();
  }

  /*
   * Shows cell title displays.
   */
  function showcells() {
    // Another way to do this would be to create
    // the cell texts once and then just hide them.
    var cellsData = d3.keys(cellsTitleX);
    var cells = svg.selectAll('.cell')
      .data(cellsData);

    cells.enter().append('text')
      .attr('class', 'cell')
      .attr('x', function (d) { return cellsTitleX[d]; })
      .attr('y', 40)
      .attr('text-anchor', 'middle')
      .text(function (d) { 

        if (cellNames.length > 15){
          if (d.replace(/\D/g,'') % 10 == 0){
            return "Cells " + (d.replace(/\D/g,'') - 9) + "-" + d.replace(/\D/g,'');
          } else {
            return "";
          }
        } else {
          return d;
        }
        
         });
  }


  /*
   * Function called on mouseover to display the
   * details of a bubble in the tooltip.
   */
  function showDetail(d) {
    // change outline to indicate hover state.
    d3.select(this).attr('stroke', 'black');
    var value = Math.round( (d.value) * 100 ) / 100;
    // if it is too little (to late ;) )
    if (value < 1){
      if (value != 0){
        value = d.value.toExponential(2);
      } else {
        value = d.value;
      }
    }
    var group = 'CPU + ' + d.group + ' pairs';
    if (d.group == 'CPU') group = d.group + ' servers';
    var content = 
                  // '<span class="name">Hardware Type: </span><span class="value">' +
                  // d.group +
                  // '</span><br/>' +
                  '<span class="name">' + d.measuredVanue +': </span><span class="value">' +
                  value +
                  ' %</span><br/>' +
                  '<span class="name">Cell: </span><span class="value">' +
                  d.cellNum +
                  '</span><br/>' + '<span class="name">Hardware Type: </span><span class="value">' +
                  group + '</span>';
    tooltip.showTooltip(content, d3.event);
  }

  /*
   * Hides tooltip
   */
  function hideDetail(d) {
    // reset outline
    d3.select(this)
      .attr('stroke', d3.rgb(fillColor(d.group)).darker());

    tooltip.hideTooltip();
  }

  /*
   * Externally accessible function (this is attached to the
   * returned chart function). Allows the visualization to toggle
   * between "single group" and "split by cell" modes.
   *
   * displayName is expected to be a string and either 'cell' or 'all'.
   */
  function toggleDisplay(displayName){
  //chart.toggleDisplay = function (displayName) {
    if (displayName === 'cell') {
      splitBubbles();
    } else {
      groupBubbles();
    }
  };

  function setupButtons() {
    d3.select('#toolbar')
      .selectAll('.bubble-button')
      .on('click', function () {
        
        // Find the button just clicked
        var button = d3.select(this);
  
        // Get the id of the button
        var buttonId = button.attr('id');
  
        // Toggle the bubble chart based on
        // the currently clicked button.
        toggleDisplay(buttonId);
        //myBubbleChart.toggleDisplay(buttonId);
      });
  }

  setupButtons();
  // return the chart function from closure.
  return chart;
}



function updateBubbles(svg, rawData){
  var radiusScale = d3.scale.linear()
  .range([2, maxRange]);

  var maxAmount = 100;
  radiusScale.domain([0, maxAmount]);
  
  // var force = d3.layout.force()
  // .size([width, height])
  // .charge(charge)
  // .gravity(-0.01)
  // .friction(0.85);
  
  svg.selectAll('circle.bubble').each(function(d) {
    for (var dat in rawData){
      if (d.id === rawData[dat].id){
        // increase only if there is significant difference
        var diff = radiusScale(+rawData[dat].total_amount)-d.radius;
        if (diff>0.5 || diff<-0.5){
          console.log('Diff:'+diff);
          console.log('new:'+ radiusScale(+rawData[dat].total_amount) + ', old: ' + d.radius);
          d.radius = radiusScale(+rawData[dat].total_amount);
          d3.select(this).transition()
          .duration(500).attr("r", d.radius);
        }
        d.value = rawData[dat].total_amount;
        
      }
    }
    
  });
  
  
}


/*
 * Helper function to convert a number into a string
 * and add commas to it to improve presentation.
 */
function addCommas(nStr) {
  nStr += '';
  var x = nStr.split('.');
  var x1 = x[0];
  var x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }

  return x1 + x2;
}



// // setup the buttons.
// setupButtons();


    })();