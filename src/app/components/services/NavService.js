(function(){
  'use strict';

  angular.module('app')
          .service('navService', [
          '$q',
          navService
  ]);

  function navService($q){
    var menuItems = [
     
      {
        name: 'Input Data',
        icon: 'input',
        class: 'material-icons md-36',
        sref: '.input'
      },
       {
        name: 'Visualization',
        icon: '',
        class: 'fa fa-line-chart fa-2x',
        sref: '.dashboard'
      }
      
    ];

    return {
      loadAllItems : function() {
        return $q.when(menuItems);
      }
    };
  }

})();
