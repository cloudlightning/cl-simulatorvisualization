(function (global) {
    'use strict';

    angular.module('app')
        .service('TidyTreeService', [
            TidyTreeService
        ]);
   

    function TidyTreeService() {
        
        var service = {
            // mouseover: mouseover,
            // mouseout : mouseout,
            update : update,
            updateNodeInfo : updateNodeInfo,
            resetTidyTree : resetTidyTree
        };

        return service;
        
    }

 
    ////////////////////////////////////// Tooltip ///////////////////////////////////////////
    /*
   * Function called on mouseover to display the
   * details of a bubble in the tooltip.
   */
  function showDetail(d, lineChartTitles, symbol) {
    // change outline to indicate hover state.
    d3.select(this).attr('stroke', 'black');
    var name = 'CPU + ' + d.name + ' pairs';
    if (d.name == 'CPU') name = d.name + ' servers';
    if (d.name == 'Cloud' || d.name.startsWith("Cell")) name = d.name;
    global.content = '<span class="name">Name: </span><span class="value">' +
                  name + '</span><br/>'+ tooltipText(d, lineChartTitles, symbol);
    global.tooltip.showTooltip(global.content, d3.event);
  }

  function tooltipText(d, lineChartTitles, symbol) {
        var text = "";
        if (d.currentIndex != null && d.treeMap){
            for (var n=0 ; n < lineChartTitles.length ; n++){
                text +=  '<span class="name">' + lineChartTitles[n] + ':  </span><span class="value">' + numberWithCommas(Math.round( (d.treeMap[lineChartTitles[n]][d.currentIndex].value) * 100 ) / 100) + " " + symbol[n] + '</span><br/>';
            }
            text +=  '<span class="name">Time: </span><span class="value">' + numberWithCommas(d.treeMap[lineChartTitles[0]][d.currentIndex].t) + ' s' + '</span><br/>';
        }
         return text;
    }

  /*
   * Hides tooltip
   */
  function hideDetail(d) {
    // reset outline
    // d3.select(this)
    //   .attr('stroke', d3.rgb(fillColor(d.group)).darker());

    global.tooltip.hideTooltip();
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////

    //// Create tidy tree /////
    function update(i, tree, diagonal, svg, data3, BubbleTooltipService, lineChartTitles, symbol, radius, strock_width, t_size, depth) {
        // tooltip
        global.tooltip = BubbleTooltipService.floatingTooltip('tidy_tooltip', 240);

        // Compute the new tree layout.
        var nodes = tree.nodes(data3[0]),
            links = tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function(d) { d.y = d.depth * depth; });

        // Declare the nodes…
        var node = svg.selectAll("g.node")
            .data(nodes, function(d) { return d.id || (d.id = ++i); });

        // Enter the nodes.
        var nodeEnter = node.enter().append("g")
            //.attr("class", "node")
            .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
            .attr("transform", function(d) { return "translate(" + project(d.x, d.y) + ")"; })
            .on('mouseover', function(d) { return showDetail(d, lineChartTitles, symbol)})
            .on('mouseout', function(d) { return hideDetail(d)})
            // .on("mouseover", function(d) {return mouseover(d, this)})
            // .on("mouseout", function(d) {return mouseout(d, this)});
            //   .attr("transform", function(d) { 
            // 	  return "translate(" + d.y + "," + d.x + ")"; });

        nodeEnter.append("circle")
            .attr("r", radius)
            .style('fill', function(d) {
                if (d.currentValue != null && d.currentValue > 0) {
                    return "#F3F781";
                } else {
                    return "#fff";
                }
            })
            .style('stroke-width', strock_width + "px");

        nodeEnter.append("text")
            .attr("class", "standard")
            .attr("dy", "0.31em")
            .attr("x", function(d) { return d.x < Math.PI === !d.children ? 13 : -13; })
            .attr("text-anchor", function(d) { return d.x < Math.PI === !d.children ? "start" : "end"; })
            .attr("transform", function(d) { return "rotate(" + (d.x < 180 ? d.x - 90 : d.x + 90) + ")"; })
            // .attr("x", function(d) { 
            //     return d.children || d._children ? -23 : 23; })
            // .attr("dy", ".35em")
            // .attr("text-anchor", function(d) { 
            //     return d.children || d._children ? "end" : "start"; })
            //.attr("transform", function(d) { return "rotate(" + (d.x < 180 ? d.x - 90 : d.x + 90) + ")"; })
            .text(function(d) { 
                var name = 'CPU + ' + d.name;
                if (d.name == 'Cloud' || d.name.startsWith("Cell") || (d.name == 'CPU')) name = d.name;
                if (d.currentValue == null) {
                    return name} 
                else {
                    return name + ": " + Math.round( d.currentValue * 100 ) / 100 + " " + d.currentSymbol;
                } 
            })
            .style("fill-opacity", 1)
            .style ("font", t_size + "px sans-serif");

        // Declare the links…
        var link = svg.selectAll("path.link")
            .data(links, function(d) { return d.target.id; });

        // Enter the links.
        link.enter().insert("path", "g")
            .attr("class", "link")
            // .attr("d", function(d) {
            //     return "M" + project(d.target.x, d.target.y)
            //         + "C" + project(d.target.x, (d.target.y + d.source.y) / 2)
            //         + " " + project(d.source.x, (d.target.y + d.source.y) / 2)
            //         + " " + project(d.source.x, d.source.y);
            .attr("d", diagonal);
            //    });
    }

    function project(x, y) {
        var angle = (x - 90) / 180 * Math.PI, radius = 1.5*y;
        return [radius * Math.cos(angle), radius * Math.sin(angle)];
    }

    // update node info each timestep (interval)
    function updateNodeInfo(svg){
        var nodeText = svg.selectAll("text.standard");
        nodeText.text(function(d) {
            var name = 'CPU + ' + d.name;
            if (d.name == 'Cloud' || d.name.startsWith("Cell") || (d.name == 'CPU')) name = d.name;
            if (d.currentValue == null) {
                return name} 
            else {
                //console.log(name + ": " + Math.round( d.currentValue * 100 ) / 100 + " " + d.currentSymbol);
                return name + ": " + Math.round( d.currentValue * 100 ) / 100 + " " + d.currentSymbol;
            } 
        });
        nodeText.style('font-weight', function(d) {
            if (d.currentValue != null && d.currentValue > 0) {
                return "bold";
            } else {
                return "normal";
            }
        });
        var circle = svg.selectAll("circle");
        circle.style('fill', function(d) {
            if (d.currentValue != null && d.currentValue > 0) {
                return "#F3F781";
            } else {
                return "#fff";
            }
        });

        


        var tidyTooltip = d3.select("#tidy_tooltip");
         if (global.tooltip.getOpacity() == 1.0){
         }


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    }

    // it is called by reset button in Charts controller
    function resetTidyTree(svg, data3){
        data3[0].currentValue = null;
        data3[0].currentSymbol = "";
        var cloudChildrenLength = data3[0].children.length; 
        for (var i=0; i<cloudChildrenLength; i++ ){
            data3[0].children[i].currentValue = null;
            data3[0].children[i].currentSymbol = "";
            var cellChildrenLength = data3[0].children[i].children.length;
            for (var j=0; j < cellChildrenLength; j++){
                data3[0].children[i].children[j].currentValue = null;
                data3[0].children[i].children[j].currentSymbol = "";
            }
        }
        if (svg) updateNodeInfo(svg);
    }

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

   
    
})(this);