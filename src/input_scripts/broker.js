/*
 *  This function is called whenever the page is loaded in order 
 *  to check if there are data to restore.
 */
	window.onload = function() { 
	
	
		// 1. Load the locally stored data
		var reloaded = sessionStorage.getItem("reloaded");
		var current_obj_cells = sessionStorage.getItem("current_obj_cells");
		var current_cells = sessionStorage.getItem("current_cells");
	
		//NEW:
		var current_hw_types_per_cell = sessionStorage.getItem("current_hw_types_per_cell");
		
		// Update the selected resource allocation type
		var resourceAllocationMechanism = sessionStorage.getItem("resourceAllocationMechanism");
		if(resourceAllocationMechanism == "1") {
			document.getElementById("radio1").checked = true;
			traditionalRadioButtonSelected();
		} else if (resourceAllocationMechanism == "2") {
			document.getElementById("radio2").checked = true
			sosmRadioButtonSelected();
		} else if (resourceAllocationMechanism == "3"){
			document.getElementById("radio3").checked = true;
			improvedSosmRadioButtonSelected();
		} else {
			// Do nothing...
		}

		//APPLICATION JSON Editor
		var current_obj = sessionStorage.getItem("current_obj");
		
		//BROKERS JSON Editor
		var current_obj_brokers = sessionStorage.getItem("current_obj_brokers");
		var current_brokers = sessionStorage.getItem("current_brokers");
	
		// 2. Check if the page is loaded after a refresh
		if (reloaded == "true") {
			// 3. Read the previously stored contents
			obj_cells = JSON.parse(current_obj_cells);
			cells = JSON.parse(current_cells);
			hw_types_per_cell = JSON.parse(current_hw_types_per_cell);
	
			// 4. Refresh the view
			if (cells.length != 0) {
				selected_cell = 1;                      
			}
			updateCellsTable();
			updateDropDownList();
	
			//APPLICATION JSON EDITOR
			// 3. Read the previously stored contents
			obj = JSON.parse(current_obj);
	
			// 4. Refresh the view
			updateApplicationTable();
			addTableRowHandler();
			
			//BROKERS JSON EDITOR
			obj_brokers = JSON.parse(current_obj_brokers);
			brokers = JSON.parse(current_brokers);
			if (brokers.length != 0) {
				selected_broker = 1;
			}
			updateBrokersTable();
			updateBrokerDropDownList();
			
			// Update the General Properties fields
			document.getElementById("max-sim-time").value = sessionStorage.getItem("maxSimTime");
			document.getElementById("update-interval").value = sessionStorage.getItem("updateInterval");
			document.getElementById("jobs-min").value = sessionStorage.getItem("minJobs");
			document.getElementById("jobs-max").value = sessionStorage.getItem("maxJobs");
			//If no Cells are defined this function call fails
			if (cells.length != 0){
				updateHwTypesTable();
			}
		}
	}
	
	/* 
	 * This function is called before reloading the page in order
	 * to store the previous work made by the user.
	 */ 
	window.onbeforeunload = function() {
	
		// 1. Define that a page refresh has been performed
		sessionStorage.setItem("reloaded", "true");
	
		// 2. Store the current work locally in the current session
		var current_obj_cells = JSON.stringify(obj_cells);
		var current_cells = JSON.stringify(cells);
		sessionStorage.setItem("current_obj_cells", current_obj_cells);
		sessionStorage.setItem("current_cells", current_cells);
	
		//NEW:
		var current_hw_types_per_cell = JSON.stringify(hw_types_per_cell);
		sessionStorage.setItem("current_hw_types_per_cell", current_hw_types_per_cell);
	
		//APPLICATION JSON EDITOR
		var current_obj = JSON.stringify(obj);
		sessionStorage.setItem("current_obj", current_obj);
		
		//BROKERS JSON EDITOR
		var current_obj_brokers = JSON.stringify(obj_brokers);
		var current_brokers = JSON.stringify(brokers);
		sessionStorage.setItem("current_brokers", current_brokers);
		sessionStorage.setItem("current_obj_brokers", current_obj_brokers);
		
		// Store the General Properties in the Session Storage
		sessionStorage.setItem("maxSimTime", document.getElementById("max-sim-time").value);
		sessionStorage.setItem("updateInterval", document.getElementById("update-interval").value);
		sessionStorage.setItem("minJobs", document.getElementById("jobs-min").value);
		sessionStorage.setItem("maxJobs", document.getElementById("jobs-max").value);
		
		// Store the selected resource allocation type
		if(document.getElementById("radio1").checked) {
			sessionStorage.setItem("resourceAllocationMechanism", "1");
		} else if (document.getElementById("radio2").checked) {
			sessionStorage.setItem("resourceAllocationMechanism", "2");
		} else if (document.getElementById("radio3").checked){
			sessionStorage.setItem("resourceAllocationMechanism", "3");
		}
		
	}
	
	// Initialize the global variables
	var obj_brokers = {"Resource allocation mechanism":"SOSM", "Brokers":[]};  
	var brokers = [];
	var selected_broker = 0;
	
	// Validaion 
	var brokers_schema = '{ "Brokers": { "items": { "properties": { "Number of functions": { "type": "integer" }, "Weights of functions": { "type": "array" }, "Number of Resources per vRM": { "type": "integer" }, "Number of vRMs per pSwitch": { "type": "integer" }, "Number of pSwitch per pRouter": { "type": "integer" }, "Poll Interval Cell Manager": { "type": "integer" }, "Poll Interval pRouter": { "type": "integer" }, "Poll Interval pSwitch": { "type": "integer" }, "Poll Interval vRM": { "type": "integer" }, "vRM deployment strategy": { "type": "integer" } }, "required": [ "Number of functions", "Weights of functions", "Number of Resources per vRM", "Number of vRMs per pSwitch", "Number of pSwitch per pRouter", "Poll Interval Cell Manager", "Poll Interval pRouter", "Poll Interval pSwitch", "Poll Interval vRM", "vRM deployment strategy" ], "type": "object" }, "type": "array" } }'
	var brokers_message = "";
	
	/**
	 * This function is called when the Parse button is clicked, and
	 * is responsible for parsing a given JSON string that contains 
	 * the details of the Brokers.
	 */
	function parseBrokersJSON() {
	
		// 0. Initialization
		selected_broker = 1;
	
		// 1. Get the JSON string
		var jsonInput = document.getElementById("import-broker-field").value;
	
		if (jsonInput.includes('{') && jsonInput.includes('}')) {
			if (validateBrokersJsonSchema(jsonInput)) {
			  
				// Parse the content of the JSON string
				obj_brokers = JSON.parse(jsonInput);
	
				// Update the brokers list and table
				updateBrokersList();
				updateBrokersTable();
				updateBrokersResourceAllocationChecklist();
				
				// Update the drop down navigation panel
				updateBrokerDropDownList();
				
			} else {
				
				// 1. Define the default error message
				var error_message = "The JSON string is not valid.\nPlease provide a valid JSON file as an input.";
	
				// 2. If the error occured due to invalid fields, provide an indicator to the user
				if (brokers_message != "") {
					error_message += "\n\nCheck the error logs below in order to identify which entries violate the JSON  schema:\n\n" + brokers_message;
				} 
	
				// 3. Display the final error message
				alert(error_message);
			}
	
		} else {
			alert("Please provide a valid json string before clicking the Parse button!");
		}     
	}
	
	/**
	 * This function is called when the Import button is clicked, and
	 * in responsile for importing a file that contains the deired JSON 
	 * string.
	 */
	function importBrokersJSONFile() {
	
		// 0. Initialization
		selected_broker = 1;
	
		// 1. Import the BrokerData to the desired object (i.e. obj_brokers)
		loadBrokersFileContent();
	
		// 2. Update the table with the Brokers data
		updateBrokersTable();
	}
	
	/**
	 * This function is responsible for parsing the content of the 
	 * file that contains the desired JSON with the Brokers data.
	 */
	function loadBrokersFileContent(){
	
		  // 1. Get the name of the desired file.
		  var fileToLoad = document.getElementById("fileToLoad-broker").files[0];
		  
		  // Create a new FileReader object.
		  var fileReader = new FileReader();
	
		  /**
		   * Define what the FileReader object should do, when the desired file is loaded.
		   */
		  fileReader.onload = function(fileLoadedEvent){
	
			  // 1. Load the String and print it to the console
			  var textFromFileLoaded = fileLoadedEvent.target.result;
			  console.log(textFromFileLoaded);
	
			  // 2. Parse the JSON string into a JavaScript object
			  if (validateBrokersJsonSchema(textFromFileLoaded)) {
				  
				  // Update the value of the Import field
				  document.getElementById("import-broker-field").value = fileToLoad["name"];
				  
				  // Parse the JSON string to the desired JavaScript object
				  obj_brokers = JSON.parse(textFromFileLoaded);
				  console.log(obj_brokers);
	
				  // Update the brokers list and table
				  updateBrokersList();
				  updateBrokersTable();
				  updateBrokersResourceAllocationChecklist();
				  
				  // Update the drop down navigation panel
				  updateBrokerDropDownList();
				  
			  } else {
				  
				// 1. Define the default error message
				var error_message = "The JSON string is not valid.\nPlease provide a valid JSON file as an input.";
	
				// 2. If the error occured due to invalid fields, provide an indicator to the user
				if (brokers_message != "") {
					error_message += "\n\nCheck the error logs below in order to identify which entries violate the JSON  schema:\n\n" + brokers_message;
				} 
	
				// 3. Display the final error message
				alert(error_message);
			  }
	
			  // 4. Clear the list with the files (to allow re-importation of the same file)
			  document.getElementById('fileToLoad-broker').value = "";
		  };
	
		  // Read the content of the file
		  fileReader.readAsText(fileToLoad, "UTF-8");
	}
	
	/**
	 * This function is responsible for marking the corresponding radio button 
     * of the checklist with the avaliable Resource Allocation Mechanisms.
	 */
	function updateBrokersResourceAllocationChecklist() {
		
		var resource = obj_brokers["Resource allocation mechanism"];
		if(resource == 'Traditional') {
			document.getElementById("radio1").checked = true;
			traditionalRadioButtonSelected();	
		} else if (resource == 'SOSM') {
			document.getElementById("radio2").checked = true
			sosmRadioButtonSelected();
		} else if (resource == 'Improved SOSM'){
			document.getElementById("radio3").checked = true;
			improvedSosmRadioButtonSelected();
		}
	}
	
	/**
	 * This function is responsible for creating an internal representation
	 * of the brokers list.
	 */
	function updateBrokersList() {
	
		// Copy the brokers list
		brokers = JSON.parse(JSON.stringify(obj_brokers["Brokers"]));
	}
	
	/**
	 * The functions below are responsible for iterating through the Brokers 
	 * that the user created or imported.
	 */
	
	function previousBroker() {
	
		// 1. Decrement the selected_broker pointer by one
		selected_broker--;
	
		// 2. Check for saturation
		if (selected_broker < 1) {
			selected_broker = 1;
		}
	
		if (brokers.length == 0) {
			selected_broker = 0;
		}
	
		// 3. Update the table in order to show the details of the selected broker
		updateBrokersTable();
	
		document.getElementById("brokers-drop-down-list").value = selected_broker;
	}
	
	function nextBroker() {
	
		// 1. Increment the selected_broker pointer by one
		selected_broker++;
	
		// 2. Check for saturation
		if (selected_broker > brokers.length) {
			selected_broker = brokers.length;
		}
	
		if (brokers.length == 0) {
			selected_broker = 0;
		}
		// 3. Update the table in order to show the details of the selected broker
		updateBrokersTable();
	
		document.getElementById("brokers-drop-down-list").value = selected_broker;
	}
	
	/**
	 * This function is called when the Export button pressed, in order
	 * to export the final list of brokers as a JSON String.
	 */
	function exportBrokerDataJSON() {
	
		if (validateJson()){
	
			// 0. Construct the final object
			if(document.getElementById("radio1").checked) {
				obj_brokers["Resource allocation mechanism"] = 'Traditional';
			} else if (document.getElementById("radio2").checked) {
				obj_brokers["Resource allocation mechanism"] = 'SOSM';
			} else if (document.getElementById("radio3").checked){
				obj_brokers["Resource allocation mechanism"] = 'Improved SOSM';
			}
			obj_brokers["Brokers"] = brokers;
	
			// 2. Convert the data into a JSON String
			var json = neatJSON(obj_brokers, {'sort':false, 'wrap':40, 'aligned':true, 'around_colon':1 });
	
			// 3. Write the string to the desired textfield
			document.getElementById("export-broker-field").value = JSON.stringify(obj_brokers);
	
		}
	
	}
	
	/**
	 * This function is responsible for updating the content of the Brokers html table
	 * with the details of the Broker object that the "selected_broker" global variable
	 * points to.         
	 */
	function updateBrokersTable() {
	
		// 1. Clean the contents of the table
		var table = document.getElementById("brokers-table");
		table.innerHTML = "";
	
		// 2. Get the selected Broker
		selObj = brokers[selected_broker - 1];
	
		// 3. Add one row for each key value found in the broker object
		var i = 1; 
		for (key in selObj) {
			
			var tr = document.createElement("tr");
	
			var t1 = document.createElement("td");
			var c1 = document.createTextNode(i)
			t1.appendChild(c1);
	
			var t2 = document.createElement("td");
			var c2 = document.createTextNode(key)
			t2.appendChild(c2);
	
			var t3 = document.createElement("td");
			var c3 = document.createTextNode(selObj[key]); 
			
			
			if (key == "vRM deployment strategy") {
				var strategy = getDeploymentStrategyName(selObj[key]);
				c3 = document.createTextNode(strategy);
				
			}
			
			t3.appendChild(c3);
			tr.appendChild(t1);
			tr.appendChild(t2);
			tr.appendChild(t3);
	
			table.appendChild(tr);
	
			i++;
		}
		
		// Extra... (Drop Down List update)
		document.getElementById("brokers-num").innerHTML = brokers.length;
		if(brokers.length == 0)  {
			document.getElementById("brokers-drop-down-list").value = "";
			document.getElementById("brokers-drop-down-list").innerHTML = "";
		}
	   
	}
	/**
	 *	This function is responsible for mapping the deployment strategy index
	 *  to its corresponding name.
	 */
	function getDeploymentStrategyName(index) {
		switch(index) {
			case 1:
				return "Task Compaction";
				break;
			case 2:
				return "Symetry Preservation";
				break;		
		}
	}
	
	/**
	 * This function is responsible for displaying an empty New/Edit Cell
	 * form.
	 */
	function newBrokerButtonPressed() {
			
		// Clear the textfields of the New/Edit form
		clearBrokerTextFields();
		
		// Update the list according to the option
		if(document.getElementById("radio1").checked) {
			traditionalRadioButtonSelected();
		} else if (document.getElementById("radio2").checked) {
			sosmRadioButtonSelected();
		} else if (document.getElementById("radio3").checked){
			improvedSosmRadioButtonSelected();
		}
		
		// Hide the Easy Nav panel
		document.getElementById('num-of-brokers').style.display = "none";
		document.getElementById('brokers-drop-down').style.display = "none";
		
		// 0. Show the Add button
		document.getElementById("brokers-buttons").style.display = "none";
		document.getElementById("button-group-3-broker").style.display = "";
		document.getElementById("save-button-broker").style.display = "none";
		document.getElementById("add-button-broker").style.display = "";
	
		// 1. Hide the table and display the form
		document.getElementById("brokers-view").setAttribute("style","display: none;");
		document.getElementById("brokers-edit").setAttribute("style","");
	
	}
	
	/**
	 * This function is responsible for clearing the fields 
	 * of the New/Edit Broker form.
	 */
	function clearBrokerTextFields() {
		
		// Get the text fields of the form
		var editSection = document.getElementById("brokers-edit");
		var fields = editSection.getElementsByTagName("input");
		
		// Clear their content
		for (var k = 1; k < fields.length; k++) {
			fields[k].value = "";
		}
	
	}
	
	/**
	 * This fucntion is called when the Cancel button is pressed
	 * in order to present the table with the details of the 
	 * Broker entries.
	 */
	function cancelBrokerButtonPressed() {
		
		// Display the Easy Nav panel
		document.getElementById('num-of-brokers').style.display = "";
		document.getElementById('brokers-drop-down').style.display = "";
		
		// Hide the buttons
		document.getElementById("button-group-3-broker").style.display = "none";
	
		// Clear the checkboxes and the text fields
		clearBrokerTextFields();
	
		// 1. Hide the New/Edit Broker form and display the table with the selected broker's contents
		document.getElementById("brokers-edit").setAttribute("style","display: none;");
		document.getElementById("brokers-view").setAttribute("style","");
		document.getElementById("brokers-buttons").setAttribute("style","");
	}
	
	/** 
	 * This function is called when the Add button is pressed and 
	 * it is responsible for adding a new Broker entry
	 * to the array with the other Brokers. The new Broker
	 * entry is verified before added to the list.
	 */
	function addBrokerButtonPressed() {
	
		// 1. Create an empty broker
		var newBroker = {};
	
		// 2. Retrieve the key values from the text fields
		newBroker = createBrokerFromFields();
	
		// 3. Check if its values are valid and add it to the list
		if (validNewBroker(newBroker)) {
	
			// Add it to the list
			brokers[brokers.length] = newBroker;
	
			// 1. Mark the new Broker as the selected one
			selected_broker = brokers.length;
	
			// 2. Update the table with the broker's details
			updateBrokersTable();
	
			// 3. Display the table with the details of the newly added broker
			cancelBrokerButtonPressed();
			updateBrokerDropDownList();
		} 
	
	}
	
	/**
	 * This function is responsible for verifying whether the 
	 * details of the given broker are valid.
	*/
	function validNewBroker(newBroker) {
	
		// 1. Assume that the details are correct
		valid = true;
		errorMessage = "";
	
		// 2. Check the details
		i = 1;
	
		// Check for NaNs
		var editSection = document.getElementById("brokers-edit");
		var fields = editSection.getElementsByTagName("input");
	
		for (var k = 0; k < fields.length; k++) {
			if ((!fields[k].readOnly) && (fields[k].value == "")) {
				valid = false;
				errorMessage += i + ". Please complete all the fields of the form. \n";
				i++;
				break;
			}
		}
		
		// Check for Numeric Values
		for (var k = 0; k < fields.length; k++) {
			if ((!fields[k].readOnly) && (isNaN(fields[k].value))) {
				valid = false;
				errorMessage += i + ". Please provide numeric values to the fields of the form. \n";
				i++;
				break;
			}
		}
		
		// Check the weights
		var weights = newBroker["Weights of functions"];
		
		for (var k = 0; k < weights.length; k++) {
			if((weights[k] < 0) || (weights[k] > 1)) {
				valid = false;
				errorMessage += i + ". The weights of the functions should be values within the interval [0.1]. \n";
				i++;
				break;
			}
		}
		
		// Check the drop down list option
		if(document.getElementById("vrm-strategy-drop-down-list").value == "") {
			valid = false;
			errorMessage += i + ". Please select one of the available vRM Deployment Strategies. \n";
			i++;
		}
		
		// 3. Check if there was at least one error
		if (!valid) {
			alert(errorMessage);
		}
	
		// 4. Return the outcome of the validation
		return valid;
	
	}
	 
	/**
	 * This function creates a new Broker object from the values
	 * provided to the form's fields by the user.
	 */
	function createBrokerFromFields() {
	
		// 1. Create an empty broker
		var newBroker = {};
	
		/*
		 * 2. Add the values from the fields
		 */     
		// Set the ID of the newly added cell
		//newBroker["BrokerID"] = brokers.length + 1;
	
		// Get the weights of each function
		
		newBroker["Number of functions"] = 5;
		newBroker["Weights of functions"] = [];
		
		if(document.getElementById("radio1").checked) {
			newBroker["Number of functions"] = 0;
		} else if (document.getElementById("radio2").checked) {
			newBroker["Number of functions"] = 5;
			
			newBroker["Weights of functions"][0] = parseFloat(document.getElementById("func-w1").value);
			newBroker["Weights of functions"][1] = parseFloat(document.getElementById("func-w2").value);
			newBroker["Weights of functions"][2] = parseFloat(document.getElementById("func-w3").value);
			newBroker["Weights of functions"][3] = parseFloat(document.getElementById("func-w4").value);
			newBroker["Weights of functions"][4] = parseFloat(document.getElementById("func-w5").value);
		} else if (document.getElementById("radio3").checked){
			newBroker["Number of functions"] = 1;
			
			newBroker["Weights of functions"][0] = parseFloat(document.getElementById("func-w1").value);
			
		}
		
		// newBroker["Number of functions"] = 5;
		// newBroker["Weights of functions"] = [];
		// newBroker["Weights of functions"][0] = parseFloat(document.getElementById("func-w1").value);
		// newBroker["Weights of functions"][1] = parseFloat(document.getElementById("func-w2").value);
		// newBroker["Weights of functions"][2] = parseFloat(document.getElementById("func-w3").value);
		// newBroker["Weights of functions"][3] = parseFloat(document.getElementById("func-w4").value);
		// newBroker["Weights of functions"][4] = parseFloat(document.getElementById("func-w5").value);
		
		// Get the other values
		newBroker["Number of Resources per vRM"] = parseInt(document.getElementById("num-res-vrm").value);
		newBroker["Number of vRMs per pSwitch"] = parseInt(document.getElementById("num-vrm-pswitch").value);
		newBroker["Number of pSwitch per pRouter"] = parseInt(document.getElementById("num-pswitch-prouter").value);
		newBroker["Poll Interval Cell Manager"] = parseInt(document.getElementById("poll-interval-cell-manager").value);
		newBroker["Poll Interval pRouter"] = parseInt(document.getElementById("poll-interval-prouter").value);
		newBroker["Poll Interval pSwitch"] = parseInt(document.getElementById("poll-interval-pswitch").value);
		newBroker["Poll Interval vRM"] = parseInt(document.getElementById("poll-interval-vrm").value);
		newBroker["vRM deployment strategy"] = parseInt(document.getElementById("vrm-strategy-drop-down-list").value);
	
		// 3. Return the Broker object
		return newBroker;
	}
	
	/**
	 * This function is called when the Delete button is pressed
	 * in order to remove the selected broker from the brokers list.
	 */
	function deleteBrokerButtonPressed() {
		
		// Ensure that a specific broker is selected by the user
		if (selected_broker != 0) {
	
			// Ask confirmation from the user to avoid accidental deletion
			var answer = confirm("This entry will be permanently deleted! Proceed with the action?");
	
			if (answer) {
	
				// 1. Remove the broker object from the array
				brokers.splice(selected_broker - 1, 1);
	
				// 2. Update the IDs of the remaining brokers
				//for (var i = 0, len = brokers.length; i < len; i++) {
				//	brokers[i]["BrokerID"] = i + 1;
				//}
	
				// 3. Display the first Broker (if not empty)
				if (brokers.length != 0) {
					selected_broker = 1;
				} else {
					selected_broker = 0;
				}
	
				// 4. Update the table and the drop down list
				updateBrokersTable();
				updateBrokerDropDownList();
			}
		} else {
			 alert("Please select a cell entry to delete!");
		}
	}
	
	/** 
	 * This function is responsible for displaying the New/Edit Cell form
	 * with its fields containing the details of the selected broker entry.
	 */
	function editBrokerButtonPressed() {
	
		// Ensure that a broker entry is selected
		if (selected_broker != 0) {
			
			// Hide the Easy Nav panel
			document.getElementById('num-of-brokers').style.display = "none";
			document.getElementById('brokers-drop-down').style.display = "none";
			
			// 0. Display the Save button
			document.getElementById("button-group-3-broker").style.display = "";
			document.getElementById("save-button-broker").style.display = "";
			document.getElementById("add-button-broker").style.display = "none";
	
			// 1. Hide the Brokers Table and present the form
			document.getElementById("brokers-view").setAttribute("style","display: none;");
			document.getElementById("brokers-edit").setAttribute("style","");
			document.getElementById("brokers-buttons").style.display = "none";
	
			// 2. Get the selected broker
			var selObj = brokers[selected_broker - 1];
			
			if(document.getElementById("radio1").checked) {
				traditionalRadioButtonSelected();
				document.getElementById("poll-interval-cell-manager").value = selObj["Poll Interval Cell Manager"];
			} else if (document.getElementById("radio2").checked) {
				sosmRadioButtonSelected();
				document.getElementById("func-w1").value = selObj["Weights of functions"][0];
				document.getElementById("func-w2").value = selObj["Weights of functions"][1];
				document.getElementById("func-w3").value = selObj["Weights of functions"][2];
				document.getElementById("func-w4").value = selObj["Weights of functions"][3];
				document.getElementById("func-w5").value = selObj["Weights of functions"][4];
				document.getElementById("num-res-vrm").value = selObj["Number of Resources per vRM"];
				document.getElementById("num-vrm-pswitch").value = selObj["Number of vRMs per pSwitch"];
				document.getElementById("num-pswitch-prouter").value = selObj["Number of pSwitch per pRouter"];
				document.getElementById("poll-interval-cell-manager").value = selObj["Poll Interval Cell Manager"];
				document.getElementById("poll-interval-prouter").value = selObj["Poll Interval pRouter"];
				document.getElementById("poll-interval-pswitch").value = selObj["Poll Interval pSwitch"];
				document.getElementById("poll-interval-vrm").value = selObj["Poll Interval vRM"];
				document.getElementById("vrm-strategy-drop-down-list").value = selObj["vRM deployment strategy"];
			} else if (document.getElementById("radio3").checked){
				improvedSosmRadioButtonSelected();
				
				document.getElementById("func-w1").value = selObj["Weights of functions"][0];
				document.getElementById("num-res-vrm").value = selObj["Number of Resources per vRM"];
				document.getElementById("num-vrm-pswitch").value = selObj["Number of vRMs per pSwitch"];
				document.getElementById("num-pswitch-prouter").value = selObj["Number of pSwitch per pRouter"];
				document.getElementById("poll-interval-cell-manager").value = selObj["Poll Interval Cell Manager"];
				document.getElementById("poll-interval-prouter").value = selObj["Poll Interval pRouter"];
				document.getElementById("poll-interval-pswitch").value = selObj["Poll Interval pSwitch"];
				document.getElementById("poll-interval-vrm").value = selObj["Poll Interval vRM"];
				document.getElementById("vrm-strategy-drop-down-list").value = selObj["vRM deployment strategy"];
			}
			// 4. Complete the text fields of the form with the details of the selected broker
			// document.getElementById("func-w1").value = selObj["Weights of functions"][0];
			// document.getElementById("func-w2").value = selObj["Weights of functions"][1];
			// document.getElementById("func-w3").value = selObj["Weights of functions"][2];
			// document.getElementById("func-w4").value = selObj["Weights of functions"][3];
			// document.getElementById("func-w5").value = selObj["Weights of functions"][4];
			// document.getElementById("num-res-vrm").value = selObj["Number of Resources per vRM"];
			// document.getElementById("num-vrm-pswitch").value = selObj["Number of vRMs per pSwitch"];
			// document.getElementById("num-pswitch-prouter").value = selObj["Number of pSwitch per pRouter"];
			// document.getElementById("poll-interval-cell-manager").value = selObj["Poll Interval Cell Manager"];
			// document.getElementById("poll-interval-prouter").value = selObj["Poll Interval pRouter"];
			// document.getElementById("poll-interval-pswitch").value = selObj["Poll Interval pSwitch"];
			// document.getElementById("poll-interval-vrm").value = selObj["Poll Interval vRM"];
			// document.getElementById("vrm-strategy-drop-down-list").value = selObj["vRM deployment strategy"];
	
		} else {
			alert("Please select a broker first!");
		}
	}
	
	/**
	 * This function is responsible for saving the changes made to the selected 
	 * broker entry.
	 */
	function saveBrokerButtonPressed() {
	
		// 1. Create a new Broker object
		var newBroker = {}
	
		// 2. Retrieve the values of the New/Edit Broker from the fields and set its ID
		newBroker = createBrokerFromFields();
		//newBroker["BrokerID"] = brokers[selected_broker - 1]["BrokerID"];
	
		// 3. Check if it is valid and update the selected broker
		if (validNewBroker(newBroker)) {
	
			// Update the selected object
			brokers[selected_broker - 1] = newBroker;
	
			// Update the Brokers Table
			updateBrokersTable();
	
			// Display the table and clear the fields
			cancelBrokerButtonPressed();
		}
	}
	
	/**
	 * These functions are responsible for exporting the final brokers list
	 * in a JSON format.
	 */
	function exportBrokersJSONtoFile2() {
		if(validateJson()) {
			// Simulate a click on the anchor 
			document.getElementById("download-brokers-anchor").click();
		}
	}
	
	function exportBrokersJSONFile() {
		
		if(validateJson()) {
		   
		   // 0. Set the number of brokers that have been defined by the user
		   //obj_cells["Number of Cells"] = obj_cells["Cells"].length;
	
		   // 1. Construct the final object
		   if(document.getElementById("radio1").checked) {
				obj_brokers["Resource allocation mechanism"] = 'Traditional';
			} else if (document.getElementById("radio2").checked) {
				obj_brokers["Resource allocation mechanism"] = 'SOSM';
			} else if (document.getElementById("radio3").checked){
				obj_brokers["Resource allocation mechanism"] = 'Improved SOSM';
			}
		   
		   obj_brokers["Brokers"] = brokers;
			
		   // 2. Turn the brokers object into a JSON format
		   var json = neatJSON(obj_brokers, {'sort':false, 'wrap':40, 'aligned':true, 'around_colon':1 });
		   
		   // 3. Set the data of the file that will be downloaded when the anchor is clicked by the user
		   document.getElementById("download-brokers-anchor").setAttribute("href", "data:application/octet-stream;," + encodeURIComponent(json));
	
		}
	}
	
	/**
	 * This function checks whether all the defined Brokers use the same Resource Allocation mechanism
	 * in a JSON format.
	 */
	function validateJson() {
		
		// 1. Retrieve the required number of functions based on the selected  Resource Allocation Mechanism
		var numOfFunctions = 0;
		
		if(document.getElementById("radio1").checked) {
			numOfFunctions = 0;
		} else if (document.getElementById("radio2").checked) {
			numOfFunctions = 5;
		} else if (document.getElementById("radio3").checked){
			numOfFunctions = 1;
		}
		
		// 2. Assume that the selection is valid
		var valid = true;
		
		// 3. Check if all the brokers were defined using the same Resource Allocation Mechanism
		for (var i = 0; i < brokers.length; i++) {
			if (brokers[i]["Number of functions"] != numOfFunctions) {
				valid = false;
				break;
			}
		}
		
		// 4. Inform the user in case their selection is invalid
		if (!valid) {
			alert("Please ensure that all the Brokers were defined using the same Resource Allocation Mechanism.");
		}
		
		// 5. Return the status of the flag "valid"
		return valid;
		
	}
	
	function exportBrokersJSONtoSimulator(){
		
		// 1. Construct the final object
		if(document.getElementById("radio1").checked) {
			obj_brokers["Resource allocation mechanism"] = 'Traditional';
		} else if (document.getElementById("radio2").checked) {
			obj_brokers["Resource allocation mechanism"] = 'SOSM';
		} else if (document.getElementById("radio3").checked){
			obj_brokers["Resource allocation mechanism"] = 'Improved SOSM';
		}
		
		obj_brokers["Brokers"] = brokers;
		
	   // 2. Turn the brokers object into a JSON format
	   var json = neatJSON(obj_brokers, {'sort':false, 'wrap':40, 'aligned':true, 'around_colon':1 });
	   document.getElementById('checkmark_div3').style.display = "inline-block";
		$.ajax({
					type: "POST",
					url: 'php/send_broker_file.php',
					data:{action: json},
					global: true,
					success:function(html) {
						brokerUploadComplete =true;
						$(".check").attr("class", "check check-complete success");
						$(".fill").attr("class", "fill fill-complete success");
						$(".path").attr("class", "path path-complete");
						var res = uploadsAreComplete();
						if (res){
							document.getElementById("run").disabled = false;
						}
					  console.log('Success!' + html);
					},
					error:function(html) {
						$(".x").attr("class", "x x-complete error");
						$(".fill").attr("class", "fill fill-complete error");
						$(".path").attr("class", "path path-complete");
						console.log('Error!' + html);
					  }
		 
			   });
	}
	
	/**
	 * This function is responsible for updating the content
	 * of the brokers dropdown list.
	 */
	function updateBrokerDropDownList() {
	
		// 1. Get the drop down list element 
		var list = document.getElementById("brokers-drop-down-list");
		
		// 2. Clear its contents
		list.innerHTML = "";
		
		// 2. Create one entry for each Cell
		for (var i = 0; i < brokers.length; i++) {
			var option = document.createElement("option");
			option.setAttribute("value", i + 1);
			option.innerHTML = i + 1 + "";
			list.appendChild(option);
		}
		
		list.value = selected_broker;
		
		if (brokers.length == 0) {
			list.value = "";
		}
	}
	
	/**
	 *  This function is responsible for displaying the details
	 *  of the broker, which is selected through the Drop Down List.
	 */
	function brokerChanged() {
	 
		// 1. Change the selected Broker
		selected_broker = parseInt(document.getElementById("brokers-drop-down-list").value);
		
		// 2. Update the Brokers table appropriatelly
		updateBrokersTable();
	
	}
	
	function validateBrokersJsonSchema(json) {
	
	 // 1. Turn the schema into a JavaScript object
	 json = JSON.parse(json);
	 schema = JSON.parse(brokers_schema);
	
	 // 2. Keep only the broker schema
	 broker_schema = schema["Brokers"]["items"];
	
	 // 4. Create the validator
	 env = new djv();
	
	 // 5. Add the schema to the validator
	 env.addSchema('broker', broker_schema);
	
	 // 6. Validate each broker entry against the schema
	 brokers_message = "";
	 var valid = true;
	 
	 // 7. Check if a completely different JSON file has been provided
	 if (json["Brokers"] == undefined) {
		 valid = false;
		 return valid;
	 }
	
	 // 8. Check if each Broker object conforms to the given schema
	 for (var i = 0; i < json["Brokers"].length; i++) {
	
		// Validate the broker against the schema
		var temp = env.validate('broker', json["Brokers"][i]);
	
		// Check if there is any violation
		if (temp != undefined) {
			valid = false; 
			brokers_message += "Broker_" + (i+1) + ": " + temp + "\n";
		} 
	 }
	
	return valid;
	}

	/**
	 * These functions are responsible for the functionality of the Resource Allocation Mechanism 
	 * group of radio buttons.
	 */
	 function traditionalRadioButtonSelected() {
		 
		 // 0. Ensure that the other two buttons are disabled
		 document.getElementById("radio2").checked = false;
		 document.getElementById("radio3").checked = false;
		 
		 // 1. Disable all the fields
		 var root = document.getElementById("brokers-edit");
		 var fields = root.getElementsByTagName("input");
		 
		 for (var i = 0; i < fields.length; i++) {
			fields[i].value = 0;
			fields[i].disabled = true;
		}
		
		document.getElementById("vrm-strategy-drop-down-list").disabled = true;
		
		// 2. Enable the desired field
		document.getElementById("poll-interval-cell-manager").disabled = false;
		document.getElementById("poll-interval-cell-manager").value = "";
	 }
	 
	 
	function sosmRadioButtonSelected() {
		 
		 // 0. Ensure that the other two buttons are disabled
		 document.getElementById("radio1").checked = false;
		 document.getElementById("radio3").checked = false;
		 
		 // 1. Enable all the fields
		 var root = document.getElementById("brokers-edit");
		 var fields = root.getElementsByTagName("input");
		 
		 for (var i = 1; i < fields.length; i++) {
			fields[i].value = "";
			fields[i].disabled = false;
		}
		
		document.getElementById("vrm-strategy-drop-down-list").disabled = false;
		
	 }
	 
	function improvedSosmRadioButtonSelected() {
		 
		 // 0. Ensure that the other two buttons are disabled
		 document.getElementById("radio1").checked = false;
		 document.getElementById("radio2").checked = false;
		 
		 // 1. Enable all the fields
		 var root = document.getElementById("brokers-edit");
		 var fields = root.getElementsByTagName("input");
		 
		 for (var i = 1; i < fields.length; i++) {
			fields[i].value = "";
			fields[i].disabled = false;
		}
		
		document.getElementById("vrm-strategy-drop-down-list").disabled = false;
		
		// 2. Disable all the weights
		var weightsField = document.getElementById("weights-id");
		var weights = weightsField.getElementsByTagName("input");
		
		for (var i = 2; i < weights.length; i++) {
			weights[i].value = 0;
			weights[i].disabled = true;
		}
		
		
	 }
	 
					