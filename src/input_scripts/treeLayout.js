    
	// 1. Global Variables
    var root1;// The global representation of the tree
    var diameter = 660; // Original value: 960
    var padding = 20;

    var tree = d3.layout.tree()
        .size([360, 270])
        .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });

    var diagonal = d3.svg.diagonal.radial()
        .projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });

    var svg = d3.select("#svg-here").append("svg")
        .attr("width", diameter)
        .attr("height", diameter)
        .append("g")
        .attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

	// Define the div of the DOM page representation where the network should be displayed
	var div = d3.select("body").append("div")
		.attr("class", "tooltip")
		.attr("id", "tooltip-id")
		.style("opacity", 1e-6);
		
		d3.selection.prototype.moveToFront = function() {
		  return this.each(function() {
			this.parentNode.appendChild(this);
		  });
		};
    
    /**
     * This function is responsible for executing the overall procedure.
     */
    d3.tsv("", function(files) {
        
      // 1. Get the data of the tree layout
      nested1 = processData(files);
        
      // 2. Hide the leaves of the tree
	  // TODO: Hide the leaves only when the number of cells exceeds a predefined threshold
	  if (nested1["children"].length > 20){  
		nested1 = hideLeaves(nested1);
	  }
	  
	  // 3. Log the initial tree
      root1 = nested1;
      
      // 3. Call the treemap function to print the tree inside the modal
      treemap(nested1);
        
    });
	
	/**
	 * This function is responsible for initially hiding the leaves 
	 * of the tree.
	 */
	function hideLeaves(data) {
		
		// Get the child nodes of the given tree
		var leave_nodes = data["children"];
		
		// For each child hide its children
		for (var i = 0; i < leave_nodes.length; i++) {
			leave_nodes[i]["_children"] = leave_nodes[i]["children"];
			leave_nodes[i]["children"] = null;
		}
		
		// Return the final tree
		return data;
	}
	
    /**
     * This function is responsible for arranging the nodes and
	 * the links of the desired data on the given canvas.
     */	 
    function processData(files) {
     
      // Print the new tree layout
      files = treeData;
       
      return burrow(files);
    }

	/**
	 * This function is responsible for displaying the final tree.
	 */
    function treemap(root) {
        
      var nodes = tree.nodes(root);
      links = tree.links(nodes);

      var link = svg.selectAll("path.link")
	     .data(links, function(d) { return d.target.id; });
      
      link
        .enter().append("path")
          .attr("class", "link")
          .attr("d", diagonal)
          .style("opacity", 0)
          .transition()//delay the display of the links - make them appear one after the other
          .duration(400)
          .delay(0)
          .style("opacity", 1);

      link.exit()
        .transition()
          .duration(400)
          .style("opacity", 0)
          .delay(0)
          .remove();

      var node = svg.selectAll(".node")
          .moveToFront()
          .data(nodes, function(d) { return d.name + "-" + (d.parent ? d.parent.name : "root");})

       node.exit()
        .transition()
          .duration(400)
          .style("opacity", 0)
          .delay(0)
          .remove();

      node
        .transition()
          .delay(0)
          .duration(800)
          .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

      node.selectAll("text")
        .transition()
          .duration(800)
          .attr("font-weight", null)
          .attr("fill", "#555")
          .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
          .attr("transform", function(d) { return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)"; })
          .text(function(d) { return d.name; });

      var g = node
        .enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })

      g.append("circle")
        .attr("r", 7)//original value: 10...
        .style("opacity", 0)
        .on("click", click)
		.on("mouseover", function(d){mouseover(d);})
		.on("mousemove", function(d){mousemove(d);})
		.on("mouseout", mouseout)
        .transition() // Make circles appear sequentially
          .duration(400)
          .delay(0)
          .style("opacity", 1);

      g.append("text")
        .attr("dy", ".31em")
        .attr("font-weight", "bold")
        .attr("fill", "black")
        .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
        .attr("transform", function(d) { return d.x < 180 ? "translate(10)" : "rotate(180)translate(-10)"; })
        .text(function(d) { return d.name; })
        .style("opacity", 0)
        .transition()// Make labels appear one after the other
          .duration(400)
          .delay(0)
          .style("opacity", 1);
        
        // Update the links…
        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
          .attr("class", "link")
          .attr("d", function(d) {
            var o = {x: root.x0, y: root.y0};
            return diagonal({source: o, target: o});
          });

        // Transition links to their new position.
        link.transition()
          .duration(400)
          .attr("d", diagonal);
    };
	
function mouseover(d) {
		document.getElementById("tooltip-id").style.display = "";
		createTooltipText(d);
			div.transition()
			//.text(createTooltipText(d))
			.duration(300)
			.style("opacity", 1);
		}
	
	/**
	 * Make the tooltip follow the mouse movement.
	 */
	function mousemove(d) {
		div
		.style("left", (d3.event.pageX + 10) + "px")
		.style("top", (d3.event.pageY + 10) + "px");
	}
	
	/**
	 * Hide the tooltip when the mouse is not pointing a node.
	 */
	function mouseout() {
		div.transition()
		.duration(300)
		.style("opacity", 1e-6);
		document.getElementById("tooltip-id").style.display = "none";
	}
	
	/**
	 *	The predefined names of the hardware types that are available
	 *  by the simulator.
	 */
	function getHardwareId(name) {
			switch(name) {
				case 'CPU':
				return 0;
				break;
				case 'GPU':
				return 1;
				break;
				case 'DFE':
				return 2;
				break;
				case 'MIC':
				return 3;
				break;
			}
	}
	
	/**
	 *	This function is responsible for updating the text of the tooltip 
	 *  according to the information of the node to which the mouse is 
	 *  pointing.
	 */
	function createTooltipText(d) {
		// Check if it is a Cell or a Hardware Type Node
		if (d.parent.name != "") {
			
			// Get the coordinates of the hardware type
			var cloudId = parseInt(d.parent.name.replace("Cell-","")) - 1;
			var hwId = getHardwareId(d.name);
			
			// Get the coresponding Hardware Device
			var hardware = hw_types_per_cell[cloudId][hwId];
			
			var innerText = '';
			
			var keys = ["HW type name", "Number of servers", "Number of CPUs per server", "Memory per server", "Storage per server", "Accelerators", "Number of accelerators per server"];

			// Keep only the desired fields
			for (var i = 0; i < keys.length; i++) {
				innerText += '<span class="name"> ' + keys[i] + ': </span>' +
							 '<span class="value">' + hardware[keys[i]] + '</span><br/>';
			}	
		
			document.getElementById("tooltip-id").innerHTML = innerText;
			
			return innerText;
			
			
			return d.name;
		} else {
			var cloudId = parseInt(d.name.replace("Cell-","")) - 1;
			var innerText = '<span class="name">Name: </span>' +
							'<span class="value">' + d.name + '</span><br/>' +
							'<span class="name"> Cell interconnection bandwidth: </span>' +
							'<span class="value">' + cells[cloudId]["Cell interconnection bandwidth"] + '</span><br/>' +
							'<span class="name"> Network bandwidth overcommitment ratio: </span>' +
							'<span class="value">' + cells[cloudId]["Network bandwidth overcommitment ratio"] + '</span><br/>'+
							'<span class="name"> Number of hardware(HW) types: </span>' +
							'<span class="value">' + cells[cloudId]["Number of hardware(HW) types"] + '</span><br/>';
											
			document.getElementById("tooltip-id").innerHTML = innerText;

			return innerText;
		}
	}
		/**
		 * This function is called when a node is clicked in order to
		 * hide or display its children.
		 */
		function click(d) {
		  // Toggle between the two states
		  if (d.children) {
			d._children = d.children;
			d.children = null;
		  } else {
			d.children = d._children;
			d._children = null;
		  }
		  
		  // Re-draw the tree
		  treemap(root1);
		}
	

    d3.select(self.frameElement).style("height", diameter + "px");